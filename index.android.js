import React from 'react'
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import mainReducer from './src/reducers';
import OnlineReport from './src/OnlineReport';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';


const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__ });


function configureStore (initialState) {
  const middleware = compose(
    applyMiddleware(
      thunk,
      loggerMiddleware,
    ),
  );
  return createStore(mainReducer, initialState, middleware);
}

const store = configureStore({});

const App = () => (
  <Provider store={store}>
    <OnlineReport />
  </Provider>
);

AppRegistry.registerComponent('OnlineReport', () => App);
