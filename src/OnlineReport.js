import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {Scene, Router, Actions} from 'react-native-router-flux';
import CompanyContainer from './containers/CompanyContainer';
import {Platform, Dimensions} from 'react-native';
import CheckContainer from './containers/CheckContainer';
import AuthContainer from './containers/AuthContainer';
import DocumentsContainer from './containers/DocumentsContainer';
import StoresSessionsContainer from './containers/StoresSessionsContainer';
import Splash from './components/Splash';
import {View, Text} from 'react-native';
import {ActionCreators} from './actions/index';
import EStyleSheet from 'react-native-extended-stylesheet';
import WorkingShiftContainer from './containers/WorkingShiftContainer';

const scenes = Actions.create(
  <Scene key='root' hideNavBar>
    <Scene key='splash' component={Splash} title='splash' type='replace' initial />
    <Scene key='auth' component={AuthContainer} title='auth' type='replace' />
    <Scene key='company' component={CompanyContainer} title='company' type='replace' />
    <Scene key='storesSessions' component={StoresSessionsContainer} title='storesSessions' type='replace'/>
    <Scene key='check' component={CheckContainer} title='check' type='replace' />
    <Scene key='workingShift' component={WorkingShiftContainer} title='workingShift' type='replace' />
    <Scene key='documents' component={DocumentsContainer} title='documents' type='replace' />
  </Scene>
);

class OnlineReport extends React.Component{
  

  componentWillMount () {
    this.props.getSession();
  }

  render(){
    
    return (
      <Router scenes={scenes} />
   );
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps (state) {
  return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(OnlineReport);

let {width} = Dimensions.get('window');
EStyleSheet.build({
  rem: width > 310 ? (width > 350 ? (width > 390 ? (width > 470 ? (width > 530 ? (width > 590 ? (width > 790 ? (width > 990 ? 36 : 32) : 30) : 28) : 26) : 22) : 18) : 15) : 12
});




