import React, { Component } from 'react';
import Splash from './Splash';
import EStyleSheet from 'react-native-extended-stylesheet';
import {
  toNumberFormat,
  toNumberFormatReciept,
  getInterval,
  getPercentage,
  getReceiptsCount,
  getAverage
} from '../lib/functions';
import ParamsModal from './ParamsModal';
import Drawer from 'react-native-drawer';
import Menu from './MenuView';
import VictoryChartView from './VictoryChartView';
import Header from './Header';
import ModalView from './Modal';
import RNExitApp from 'react-native-exit-app';
import c from '../lib/const';
import Estyles from '../lib/styles'
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  Modal,
  BackHandler,
  ImageBackground,
  ScrollView,
  Platform
} from 'react-native';

export default class extends Component {

  constructor(props) {
    super(props);
    this.backHandler = this.onBackButton.bind(this);
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backHandler);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
  }

  onBackButton() {
    if (this.props.company.menu == true) {
      this.props.closeMenu()
    } else {
      console.log('exitApp')
      RNExitApp.exitApp();
    }
  }

  createListOfCompany(shopsCH, shops) {
    console.log('createListOfCompany')
    var listOfCompany = []
    console.log('listOfCompany', listOfCompany)
    if (shopsCH.length !== 0 && shops.length !== 0) {
      shopsCH.forEach((item, i) => {
        listOfCompany.push({
          storeUuid: item.storeUuid,
          receiptsCount: item.receiptsCount,
          total: item.total,
          average: item.average,
          name: item.name,
          address: item.address
        })
      })
      shops.forEach((it, i) => {
        if (listOfCompany.every((elem) => elem.storeUuid !== it.uuid)) {
          listOfCompany.push({
            storeUuid: it.uuid,
            receiptsCount: 0,
            total: 0,
            average: 0,
            name: it.name,
            address: it.address
          })
        }
      })
    } else if (shopsCH.length !== 0 && shops.length == 0) {
      shopsCH.forEach((item, i) => {
        listOfCompany.push({
          storeUuid: item.storeUuid,
          receiptsCount: item.receiptsCount,
          total: item.total,
          average: item.average,
          name: item.name,
          address: item.address
        })
      })
    } else if (shopsCH.length == 0 && shops.length !== 0) {
      shops.forEach((item, i) => {
        listOfCompany.push({
          storeUuid: item.uuid,
          receiptsCount: 0,
          total: 0,
          average: 0,
          name: item.name,
          address: item.address
        })
      })
    } else {
      listOfCompany = null
    }
    this.props.addListOfShops(listOfCompany)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.session.token && (this.props.session.token !== nextProps.session.token)) {
      console.log('token')
      this.props.saveSession(nextProps.session);
      this.props.getCompany(nextProps.session.token, nextProps.session.intervalState);
      this.props.getAllShopsGraphData(nextProps.session.token, nextProps.session.intervalState)
    }

    if ((this.props.session.token === nextProps.session.token) && (nextProps.session.intervalState !== this.props.session.intervalState)) {
      console.log('intervalState')
      this.props.saveSession(nextProps.session);
      this.props.getCompany(this.props.session.token, nextProps.session.intervalState);
      this.props.getAllShopsGraphData(this.props.session.token, nextProps.session.intervalState)
      this.props.closeModal();
    }
    if (nextProps.company.shops.length !== 0 && (nextProps.company.shopsClickHouse && (this.props.company.shopsClickHouse !== nextProps.company.shopsClickHouse))) {
      this.createListOfCompany(nextProps.company.shopsClickHouse.shops, nextProps.company.shops)
    }
    if (this.props.company.listOfShop !== nextProps.company.listOfShop && nextProps.company.listOfShop && nextProps.company.listOfShop.length == 1) {
      this.props.getStoresSessions(this.props.session.token, nextProps.company.listOfShop[0], this.props.session.intervalState)
    }
    if (nextProps.company.error && (this.props.company.error !== nextProps.company.error)
      || (nextProps.company.listOfShop == null && (nextProps.company.isAdd && nextProps.company.isAdd !== this.props.company.isAdd) && nextProps.company.shopsClickHouse)) {
      this.props.setModalState('noCompany');
      this.props.openModal();
      this.props.notIsAdd();
    }
    if (nextProps.company.modalState === 'exit' && (this.props.company.modalState !== nextProps.company.modalState)) {
      this.props.openModal();
    }
    if (nextProps.company.isLoading == false && (this.props.company.isLoading !== nextProps.company.isLoading)){
      this.props.saveSession(nextProps.session);
    }
  }

  onDotsButton() {
    this.props.setModalState('dots');
    this.props.openModal();
  }

  renderItem(row) {
    if (row) {
      return row.map((item, i) => (
        <View key={i} style={styles.orangeShop}>
          <TouchableOpacity
            style={Estyles.shop}
            onPress={() => this.props.getStoresSessions(this.props.session.token, item, this.props.session.intervalState)}>
            <View style={styles.leftHalf}>
              <Text style={styles.textName} numberOfLines={1}>{item.name}</Text>
              <Text style={styles.textAddress} numberOfLines={1}>{item.address}</Text>
            </View>
            <View style={styles.rightHalf}>
              <Text style={styles.textShopAmount}>{toNumberFormat(item.total)}</Text>
            </View>
          </TouchableOpacity>
        </View>
      ))
    }
  }

  render() {
    var buttonMenu =
      <TouchableOpacity style={Estyles.icon}
        hitSlop={{ bottom: 0, top: 0, right: 1.5 * c.rem, left: 2 }}
        onPress={() => this.props.openMenu()}>
        <Image source={require('../images/menu.png')}
          style={{ width: 1.2 * c.rem, height: c.rem }} />
      </TouchableOpacity>

    if (this.props.company.modalState == 'exit') {
      var modalText =
        <Text style={styles.modalText}>Вы уверены что хотите выйти из учетной записи?</Text>
      var modalButton =
        <TouchableOpacity style={styles.activeButton} onPress={() => this.props.onLogout()}>
          <Text style={styles.activeText} >Выйти</Text>
        </TouchableOpacity>
    } else if (this.props.company.modalState == 'noCompany') {
      if (this.props.company.error) {
        var errorContainer = <Text style={styles.modalText}>{this.props.company.error}</Text>
      }
      var modalText =
        <View>
          <Text style={styles.modalText}>Нет магазинов</Text>
          {errorContainer}
        </View>
      var modalButton =
        <TouchableOpacity style={styles.activeButton} onPress={() => { this.props.closeModal(), this.props.getCompany(this.props.session.token, this.props.session.intervalState), this.props.getAllShopsGraphData(this.props.session.token, this.props.session.intervalState) }}>
          <Text style={styles.activeText}>Перезагрузить</Text>
        </TouchableOpacity>
    }

    if ((this.props.company.listOfShop == null && this.props.company.isLoading) || this.props.storesSessions.isLoading || this.props.company.isLoading) {
      var view =
        <View style={{ flex: 1 }}>
          <Splash />
        </View>
    } else if (this.props.company.listOfShop == null && !this.props.company.isLoading) {
      view =
        <Drawer
          open={this.props.company.menu}
          type='overlay'
          content={<Menu {...this.props} />}
          tapToClose={false}
          openDrawerOffset={0.2} // 20% gap on the right side of drawer
          panCloseMask={0.1}
          tweenDuration={300}
          ref={(ref) => this._drawer}
          onOpenStart={() => {
            console.log('onopen');
            this.props.openMenu();
          }}
          onCloseStart={() => {
            console.log('onclose');
            this.props.closeMenu();
          }}
          negotiatePan
          captureGestures
          panOpenMask={0.1}>
          <ModalView
            {...this.props}
            modalText={modalText}
            modalButton={modalButton} />
          <ImageBackground source={require('../images/authBack.png')}
            style={{ width: c.WIDTH, height: c.HEIGHT }}>
            <Header
              buttonBack={buttonMenu}
              headerText='Ваши магазины' />
          </ImageBackground>
        </Drawer>
    }
    if (this.props.company.isLoading) {
      var view =
        <View style={{ flex: 1 }}>
          <Splash />
        </View>
    } else {
      if (this.props.company.listOfShop && this.props.company.listOfShop.length > 1) {
        if (this.props.company.shopsClickHouse.inCompareWithLastInterval) {
          var datas = this.props.company.shopsClickHouse.inCompareWithLastInterval
          var percentage = getPercentage(datas)
          var receiptsCount = getReceiptsCount(datas)
          var average = getAverage(datas)
        }
        if (this.props.company.modalState !== 'exit' && this.props.company.modalState !== 'noCompany') {
          var modal =
            <ParamsModal {...this.props} />
        } else {
          var modal =
            <ModalView
              {...this.props}
              modalText={modalText}
              modalButton={modalButton} />
        }
        var buttonDots =
          <TouchableOpacity
            style={Estyles.icon}
            onPress={() => this.onDotsButton()}>
            <Image source={require('../images/dots.png')}
              style={{ width: 1.15 * c.rem, height: 0.8 * c.rem }} />
          </TouchableOpacity>


        // var buttonRefresh =
        //   <TouchableOpacity
        //     style={Estyles.icon}
        //     onPress={() => {alert('on process')}}>
        //     <Image source={require('../images/circular-arrow.png')}
        //       style={{ width: 1.15 * c.rem, height: 0.8 * c.rem }} />
        //   </TouchableOpacity>

        var buttonRefresh =
        <TouchableOpacity style={Estyles.icon}
          onPress={() => this.props.getCompanyUpdate(this.props.session.token)}>
          <Image source={require('../images/circular-arrow.png')}
            style={{ width: 1 * c.rem, height: 1.1 * c.rem }} />
        </TouchableOpacity>

        var view =
          <Drawer
            open={this.props.company.menu}
            type='overlay'
            content={<Menu {...this.props} />}
            tapToClose={false}
            openDrawerOffset={0.2} // 20% gap on the right side of drawer
            panCloseMask={0.1}
            tweenDuration={300}
            ref={(ref) => this._drawer}
            onOpenStart={() => {
              console.log('onopen');
              this.props.openMenu();
            }}
            onCloseStart={() => {
              console.log('onclose');
              this.props.closeMenu();
            }}
            negotiatePan
            captureGestures
            panOpenMask={0.1}>
            {modal}
            <Header
              buttonBack={buttonMenu}
              headerText='Ваши магазины'
              intervalText={getInterval(this.props.session.intervalState, 'Выручка')}
              buttonRefresh={buttonRefresh}
              buttonRight={buttonDots} />
            <ScrollView>
              <View style={Estyles.shopListContainer}>
                <View style={Estyles.allShopsContainer}>
                  <View style={styles.allShops}>
                    <View style={styles.headerRow}>
                      <View style={styles.numberContainer}>
                        <Text style={styles.textHeaderAmount}>{this.props.company.shopsClickHouse.allShopsData && this.props.company.shopsClickHouse.allShopsData.total
                          ? toNumberFormat(this.props.company.shopsClickHouse.allShopsData.total) : 'Нет данных'}
                        </Text>
                        {percentage}
                      </View>
                      <VictoryChartView {...this.props} source={this.props.company} />
                      <View style={styles.row}>
                        <View style={styles.column}>
                          <View style={styles.numberContainer}>
                            <Text style={styles.textAmount}>{this.props.company.shopsClickHouse.allShopsData && this.props.company.shopsClickHouse.allShopsData.receiptsCount
                              ? toNumberFormatReciept(this.props.company.shopsClickHouse.allShopsData.receiptsCount) : '--'}</Text>
                            {receiptsCount}
                          </View>
                          <Text style={styles.textLabel}>Чеков</Text>
                        </View>
                        <View style={styles.column}>
                          <View style={styles.numberContainer}>
                            <Text style={styles.textAmount}>{this.props.company.shopsClickHouse.allShopsData && this.props.company.shopsClickHouse.allShopsData.average
                              ? toNumberFormat(this.props.company.shopsClickHouse.allShopsData.average) : '--'}</Text>
                            {average}
                          </View>
                          <Text style={styles.textLabel}>Средний чек</Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                {this.renderItem(this.props.company.listOfShop)}
              </View>
            </ScrollView>
          </Drawer>
      }
    }
    return (
      <View style={{ flex: 1 }}>
        {view}
      </View>
    )
  }
}

var styles = EStyleSheet.create({

  orangeShop: {
    paddingTop: '0.1rem',
    marginTop: '0.2rem',
    marginBottom: '0.5rem',
    backgroundColor: c.ORANGE,
    borderRadius: '0.3rem'
  },
  allShops: {
    flex: 1,
    justifyContent: 'space-around'
  },
  leftHalf: {
    justifyContent: 'space-between',
    flex: 5,
    alignItems: 'flex-start'
  },
  rightHalf: {
    flex: 2,
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  textName: {
    fontFamily: c.UbuntuR,
    fontSize: '0.6rem',
    color: c.BLACK
  },
  textAmount: {
    fontFamily: c.RubikR,
    fontSize: '0.8rem',
    color: c.BLACK,
    marginRight: '0.3rem'
  },
  textShopAmount: {
    fontFamily: c.RubikR,
    fontSize: '0.6rem',
    color: c.BLACK,
    marginBottom: '0.3rem'
  },
  textAddress: {
    fontFamily: c.UbuntuR,
    fontSize: '0.5rem',
    color: c.SILVER
  },
  headerRow: {
    flex: 1,
    flexDirection: 'column',
    paddingRight: 'rem',
    paddingTop: '0.5rem',
    justifyContent: 'space-around'
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingLeft: '1.5rem',
    paddingRight: 'rem',
    justifyContent: 'space-around'
  },
  column: {
    flex: 2,
    justifyContent: 'space-around',
    paddingLeft: '2.5rem'
  },
  textLabel: {
    marginTop: '0rem',
    fontFamily: c.UbuntuR,
    fontSize: '0.5rem',
    color: c.SILVER
  },
  textHeaderAmount: {
    fontFamily: c.RubikR,
    fontSize: '1.2rem',
    color: c.BLACK,
    marginLeft: '1rem',
  },
  underlay: {
    flex: 1,
    backgroundColor: 'rgba(31,97,201,0.5)'
  },
  modalContainer: {
    flex: 5,
    marginTop: '15rem',
    marginBottom: '0.5rem',
    marginLeft: 'rem',
    marginRight: 'rem',
    backgroundColor: c.WHITE,
    borderRadius: '0.3rem'
  },
  activeText: {
    textAlign: 'center',
    fontFamily: c.UbuntuL,
    fontSize: '0.7rem',
    color: c.WHITE,
    marginBottom: '0.3rem'
  },
  activeButton: {
    paddingTop: '0.3rem',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: c.BLUE,
    borderRadius: '0.5rem',
    height: '2rem',
    width: '10rem'
  },
  modalText: {
    textAlign: 'center',
    fontFamily: c.UbuntuL,
    fontSize: 'rem',
    color: c.DGREY,
    marginBottom: '0.3rem'
  },
  numberContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginBottom: '0.5rem'
  }
})
