import React, { Component } from 'react';
import c from '../lib/const';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import EStyleSheet from 'react-native-extended-stylesheet';
import { errorCase } from '../lib/errorCase'
import {
  Platform,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  Modal,
  ActivityIndicator
} from 'react-native';

const cutSpaces = (value) => {
  if (!value) {
    return value;
  }
  let newValue = value.replace(' ', '');
  return newValue;
};

export default class AuthView extends Component {

  onLinkButton() {
    this.props.setModalState('link');
    this.props.openModal(); 
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.error == 400 && (this.props.auth.error !== nextProps.auth.error)) {
      this.props.setModalState('authError');
      this.props.openModal();
    }
  }

  render() {
    if (this.props.auth.error && this.props.auth.error !== 400) {
      var error = <Text style={styles.errorText}>{errorCase(this.props.auth.error)}</Text>;
    }
    if (this.props.auth.isLoading) {
      var enter = <ActivityIndicator
        size='small'
        color={c.WHITE} />;
    } else {
      var enter = <Text style={styles.enterText}>ВОЙТИ</Text>;
    }

    if (this.props.auth.isPasswordHidden) {
      var eye =
        <Image source={require('../images/eye.png')}
          style={{ width: 1.3 * c.rem, height: 0.99 * c.rem }} />;
    } else {
      eye =
        <Image source={require('../images/blueEye.png')}
          style={{ width: 1.3 * c.rem, height: 0.99 * c.rem }} />;
    }

    if (this.props.auth.modalState == 'link') {
      var modalText =
        <Text style={styles.modalText}>Для получения логина и пароля зайдите на вкладку "Настройки" страницы приложения в личном кабинете Эвотор</Text>
    } else if (this.props.auth.modalState == 'authError') {
      modalText =
        <Text style={styles.modalText}>Неверный логин или пароль</Text>
    }

    var modalView =
      <View style={styles.underlay}>
        <View style={styles.modalContainer}>
          <View style={styles.modalCloseArea}>
            <TouchableOpacity style={styles.closeButton}
              onPress={() => this.props.closeModal()}>
              <Image source={require('../images/close.png')}
                style={{ width: 0.7 * c.rem, height: 0.7 * c.rem }} />
            </TouchableOpacity>
          </View>
          <View style={styles.modalTextArea}>
            {modalText}
          </View>
        </View>
      </View>

    return (
      <KeyboardAwareScrollView
        scrollEnabled={false}
        alwaysBounceVertical={false}>
        <ImageBackground source={require('../images/splash.png')}
          style={{ width: c.WIDTH, height: c.HEIGHT, flex: 1 }}>
          <View style={styles.topShopHalfContainer} />
          <Modal
            animationType={'fade'}
            transparent
            visible={this.props.auth.modalVisible}
            onRequestClose={() => { this.props.closeModal() }}>
            {modalView}
          </Modal>
          <View style={styles.bottomHalfContainer}>
            <View style={styles.whiteBubble}>
              <View style={styles.bubbleIconArea}>
                <Image source={require('../images/login.png')}
                  style={{ width: 1 * c.rem, height: 1.075 * c.rem }} />
              </View>
              <View style={styles.bubbleTextArea}>
                <Text style={styles.labelText}>Логин</Text>
                <TextInput
                  placeholder={'Логин'}
                  autoCapitalise='none'
                  autoCorrect={false}
                  style={styles.textInput}
                  onChangeText={(text) => this.props.setLogin(text)}
                  value={cutSpaces(this.props.auth.login)}
                  underlineColorAndroid='transparent'
                  returnKeyType='next' />
              </View>
              <View style={styles.bubbleIconArea} />
            </View>
            <View style={styles.whiteBubble}>
              <View style={styles.bubbleIconArea}>
                <Image source={require('../images/password.png')}
                  style={{ width: 1 * c.rem, height: 1.228 * c.rem }} />
              </View>
              <View style={styles.bubbleTextArea}>
                <Text style={styles.labelText}>Пароль</Text>
                <TextInput
                  placeholder={'********'}
                  style={styles.textInput}
                  onChangeText={(text) => this.props.setPassword(text)}
                  value={cutSpaces(this.props.auth.password)}
                  secureTextEntry={this.props.auth.isPasswordHidden}
                  underlineColorAndroid='transparent' />
              </View>
              <TouchableOpacity style={styles.bubbleIconArea}
                onPress={() => this.props.toggleEye()}>
                {eye}
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => this.props.onAuth(this.props.auth.login, this.props.auth.password)}>
              <View style={styles.blueBubble}>
                {enter}
              </View>
            </TouchableOpacity>
            {error}
            <TouchableOpacity style={styles.registerLink}
              onPress={() => this.onLinkButton()}>
              <View style={{ backgroundColor: 'rgba(0,0,0,0)' }}>
                <Text style={styles.linkText}>Нет логина и пароля?</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </KeyboardAwareScrollView>
    )
  }
}

var styles = EStyleSheet.create({
  topShopHalfContainer: {
    paddingTop: '3rem',
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bubbleIconArea: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  underlay: {
    flex: 1,
    backgroundColor: 'rgba(52,52,52,0.5)'
  },
  modalContainer: {
    flex: 1,
    marginTop: '10rem',
    marginBottom: '5rem',
    marginLeft: 'rem',
    marginRight: 'rem',
    padding: '1.5rem',
    backgroundColor: c.WHITE,
    borderRadius: '0.4rem'
  },
  closeButton: {
    backgroundColor: c.TBLUE,
    borderRadius: '2rem',
    height: '2rem',
    width: '2rem',
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalCloseArea: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  modalTextArea: {
    marginLeft: '1.5rem',
    marginRight: '1.5rem',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalText: {
    textAlign: 'center',
    fontFamily: c.UbuntuL,
    fontSize: 'rem',
    color: c.DGREY,
    marginBottom: '3rem'
  },
  errorText: {
    fontFamily: c.UbuntuR,
    fontSize: '0.55rem',
    color: c.RED
  },
  bottomHalfContainer: {
    flex: 4,
    marginTop: '0.5rem',
    marginBottom: '1rem',
    marginLeft: '1rem',
    marginRight: '1rem'
  },
  whiteBubble: {
    flexDirection: 'row',
    borderRadius: '0.3rem',
    marginBottom: '0.6rem',
    borderColor: c.LGREY,
    borderWidth: 1.5,
    backgroundColor: c.WHITE,
    paddingBottom: Platform.OS === 'ios' ? '1.5rem' : 0,
    flex: 4
  },
  bubbleTextArea: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },
  blueBubble: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '0.3rem',
    backgroundColor: c.ORANGE,
    height: '3rem'
  },
  labelText: {
    fontFamily: c.UbuntuR,
    fontSize: '0.6rem',
    color: c.GREY
  },
  textInput: {
    padding: '0.1rem',
    width: '9rem',
    height: '1.8rem',
    fontFamily: c.UbuntuR,
    fontSize: '0.7rem',
    color: c.BLACK
  },
  enterText: {
    fontFamily: c.UbuntuM,
    fontWeight: 'bold',
    fontSize: '0.9rem',
    color: c.WHITE
  },
  registerLink: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  linkText: {
    fontFamily: c.UbuntuR,
    fontSize: '0.55rem',
    color: c.DGREY
  }
});
