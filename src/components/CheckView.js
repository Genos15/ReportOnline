import React, { Component } from 'react';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import EStyleSheet from 'react-native-extended-stylesheet';
import { toNumberFormat } from '../lib/functions';
import moment from 'moment';
import Drawer from 'react-native-drawer';
import Menu from './MenuView';
import c from '../lib/const';
import { Actions } from 'react-native-router-flux';
import ModalView from './Modal';
import Header from './Header';
import {
  View,
  Text,
  TouchableOpacity,
  BackHandler,
  ScrollView,
  Image,
  Modal,
  ImageBackground,
  Platform
} from 'react-native';


export default class CheckView extends Component {
  constructor(props) {
    super(props);
    this.backHandler = this.onBackButton.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backHandler);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.company.modalState === 'exit' && (this.props.company.modalState !== nextProps.company.modalState)) {
      this.props.openModal();
    }
  }

  onBackButton() {
    if (this.props.documents.fromDocuments){
      this.props.getDocuments(this.props.session.token, this.props.session.intervalDocumentsState)
      return true;
    } else {
    Actions.workingShift()
    return true;
    }
  }
  render() {
    const tableHead = ['Товар', 'Цена', 'Кол-во', 'Стоимость'];
    const tableData = []
    if (this.props.check.selectCheck.items) {
      this.props.check.selectCheck.items.forEach((item) => {
        tableData.push([item.productName.substr(0, 50), item.price, item.quantity.toFixed(2), item.cost.toFixed(2)])
      })
    }
    var buttonBack =
      <TouchableOpacity style={styles.icon}
        hitSlop={{ bottom: 0, top: 0, right: 1.5 * c.rem, left: 2 }}
        onPress={() => this.onBackButton()} >
        <Image source={require('../images/back.png')}
          style={{ width: 0.8 * c.rem, height: 1.3 * c.rem }} />
      </TouchableOpacity>

    if (this.props.check.selectCheck.paymentType === 'CARD') {
      var image = <Image source={require('../images/card.png')}
        style={{ width: 1 * c.rem, height: 1 * c.rem, marginRight: c.rem }} />
    } else {
      image = <Image source={require('../images/coins.png')}
        style={{ width: 1 * c.rem, height: 1 * c.rem, marginRight: c.rem }} />
    }
    var modalText =
      <Text style={styles.modalText}>Вы уверены что хотите выйти из учетной записи?</Text>
    var modalButton =
      <TouchableOpacity style={styles.activeButton} onPress={() => this.props.onLogout()}>
        <Text style={styles.activeText} >Выйти</Text>
      </TouchableOpacity>
    return (
      <Drawer
        open={this.props.company.menu}
        type='overlay'
        content={<Menu {...this.props} />}
        tapToClose={false}
        openDrawerOffset={0.2} // 20% gap on the right side of drawer
        panCloseMask={0.1}
        tweenDuration={300}
        ref={(ref) => this._drawer}
        onOpenStart={() => {
          console.log('onopen');
          this.props.openMenu();
        }}
        onCloseStart={() => {
          console.log('onclose');
          this.props.closeMenu();
        }}
        negotiatePan
        captureGestures
        panOpenMask={0.1}>
        <ModalView
          {...this.props}
          modalText={modalText}
          modalButton={modalButton} />
        <Header
          buttonBack={buttonBack}
          headerText={this.props.storesSessions.selectShop.name}
          intervalText={`Чек номер: ${this.props.check.selectCheck.number}`} />
        <ImageBackground source={require('../images/authBack.png')} style={{ width: c.WIDTH, height: c.HEIGHT, flex: 1 }}>
          <View style={styles.shopListContainer}>
            <View style={styles.orangeShop}>
              <View style={styles.shop}>
                <View style={styles.leftHalf}>
                  <Text style={styles.textShopAmount}>Сумма {toNumberFormat(this.props.check.selectCheck.closeResultSum)}</Text>
                </View>
                {image}
                <View style={styles.rightHalf}>
                  <Text style={styles.textName}>{moment(this.props.check.selectCheck.date).format('DD-MM-YYYY HH:mm')}</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <ScrollView>
              <Table borderStyle={{ borderWidth: 0, borderColor: c.WHITE }} style={styles.table}>
                <Row data={tableHead} flexArr={[3, 1, 1, 1]} style={styles.head} textStyle={styles.text} />
                {
                  tableData.map((data, i) => (
                    <Row key={i} data={data} borderStyle={{ borderWidth: 0, borderColor: c.WHITE }} style={[styles.list, i % 2 && { backgroundColor: 'rgba(255, 163, 50, 0.1)' }]} flexArr={[3, 1, 1, 1]} textStyle={styles.text} />
                  ))
                }
              </Table>
            </ScrollView>
          </View>
        </ImageBackground>
      </Drawer>
    )
  }
}

var styles = EStyleSheet.create({
  head: {
    height: 40,
    backgroundColor: 'rgba(255, 163, 50, 0.1)'
  },
  text: {
    margin: 5,
    fontFamily: c.UbuntuR,
    fontSize: '0.7rem',
    color: c.BLACK
  },
  table: {
    backgroundColor: c.WHITE
  },
  icon: {
    height: '3rem',
    justifyContent: 'center',
    alignItems: 'center',
    width: '3rem',
    paddingLeft: '0.5rem'
  },
  shopListContainer: {
    paddingBottom: '0.5rem',
    paddingLeft: '0.5rem',
    paddingRight: '0.5rem',
    backgroundColor: c.LGREY,
    height: c.HEIGHT / 8
  },
  shop: {
    height: c.HEIGHT / 10,
    flexDirection: 'row',
    marginTop: '0.06rem',
    padding: '0.8rem',
    backgroundColor: c.WHITE,
    borderRadius: '0.3rem'
  },
  orangeShop: {
    flex: 1,
    paddingTop: '0.1rem',
    marginTop: '0.2rem',
    marginBottom: '0.5rem',
    backgroundColor: c.ORANGE,
    borderRadius: '0.3rem'
  },
  leftHalf: {
    justifyContent: 'space-between',
    flex: 5,
    alignItems: 'flex-start'
  },
  rightHalf: {
    flex: 2,
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  textName: {
    fontFamily: c.UbuntuR,
    fontSize: '0.5rem',
    color: c.BLACK
  },
  textShopAmount: {
    fontFamily: c.RubikR,
    fontSize: '0.6rem',
    color: c.BLACK,
    marginBottom: '0.3rem'
  },
  modalText: {
    textAlign: 'center',
    fontFamily: c.UbuntuL,
    fontSize: 'rem',
    color: c.DGREY,
    marginBottom: '0.3rem'
  },
  activeText: {
    textAlign: 'center',
    fontFamily: c.UbuntuL,
    fontSize: '0.7rem',
    color: c.WHITE,
    marginBottom: '0.3rem'
  },
  activeButton: {
    paddingTop: '0.3rem',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: c.BLUE,
    borderRadius: '0.5rem',
    height: '2rem',
    width: '10rem'
  },
});
