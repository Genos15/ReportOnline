import React, { Component } from 'react';
import {
  Image,
  View,
  ActivityIndicator,
  ImageBackground
} from 'react-native';
import c from '../lib/const';

export default class Splash extends Component {
  render() {
    return (
      <ImageBackground source={require('../images/authBack.png')}
        style={{ width: c.WIDTH, height: c.HEIGHT }} >
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <ActivityIndicator
            size='large'
            color={c.DEEPBLUE} />
        </View>
      </ImageBackground>

    );
  }
}
