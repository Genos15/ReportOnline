import React, { Component } from 'react';
import {
    Platform,
    Text,
    View,
    TouchableOpacity,
    Image,
    ImageBackground,
    Modal,
    BackHandler,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Actions } from 'react-native-router-flux';
import Estyles from '../lib/styles'
import c from '../lib/const';


export default class Header extends Component{
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Modal
            animationType={'fade'}
            transparent
            visible={this.props.company.modalVisible}
            onRequestClose={() => { this.props.closeModal() }}>
            <View style={Estyles.underlayExit}>
                <View style={Estyles.modalContainerExit}>
                    <View style={styles.modalCloseArea}>
                        <TouchableOpacity style={styles.closeButton}
                            onPress={() => this.props.closeModal()}>
                            <Image source={require('../images/close.png')}
                                style={{ width: 0.7 * c.rem, height: 0.7 * c.rem }} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.modalTextArea}>
                        {this.props.modalText}
                    </View>
                    <View style={styles.modalButtonArea}>
                        {this.props.modalButton}
                    </View>
                </View>
            </View>
            </Modal>
        )
    }
}

var styles = EStyleSheet.create({
    modalCloseArea: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
      },
      closeButton: {
        backgroundColor: c.TBLUE,
        borderRadius: '2rem',
        height: '2rem',
        width: '2rem',
        justifyContent: 'center',
        alignItems: 'center'
      },
      modalTextArea: {
        marginLeft: '1.5rem',
        marginRight: '1.5rem',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
      modalButtonArea: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
})