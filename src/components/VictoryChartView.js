import { VictoryChart, VictoryLine, VictoryTheme, VictoryBar, VictoryAxis, VictoryArea } from "victory-native";
import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Image,
    Modal,
    BackHandler,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Actions } from 'react-native-router-flux';
import c from '../lib/const';
import moment from 'moment';
import 'moment/locale/ru';

export default class ModalView extends Component{
    constructor(props) {
        super(props);
    }
    onExitButton() {
        this.props.closeModal();
        this.props.setModalState('exit');
    }
    render() {
        if (this.props.source.graphData && this.props.source.graphData.points && this.props.source.graphData.points[0]) {
            var max = 0;
            var maxLine;
            this.props.source.graphData.points[0].forEach((item) => {
              if (item.y > max) {
                max = item.y;
              }
            }
            );
            switch (true) {
              case max < 100:
                maxLine = 100;
                break;
              case max >= 100 && max < 1500:
                maxLine = Math.ceil(max / 100) * 100;
                break;
              case max >= 1500 && max < 4000:
                if (max % 1000 < 500) {
                  maxLine = Math.ceil(max / 1000) * 1000 - 500;
                } else {
                  maxLine = Math.ceil(max / 1000) * 1000;
                }
                break;
              case max >= 4000 && max < 20000:
                maxLine = Math.ceil(max / 1000) * 1000;
                break;
              case max >= 20000 && max < 40000:
                if (max % 10000 < 5000) {
                  maxLine = Math.ceil(max / 10000) * 10000 - 5000;
                } else {
                  maxLine = Math.ceil(max / 10000) * 10000;
                }
                break;
              case max >= 40000:
                maxLine = Math.ceil(max / 10000) * 10000;
                break;
            }
            if (this.props.source.graphData.step === 'hours') {
              var labelFunction = (v) => {
                let d = moment(this.props.source.graphData.sessionBeginsAt, 'YYYY-MM-DD HH');
                let x = d.add((v * 1), this.props.source.graphData.step);
                return (v % 4 === 0) ? x.format(this.props.source.graphData.step === 'days' ? 'DD.MM' : 'H') : null;
              };
              var tickCount = 24
            } else if (this.props.source.graphData.points[0].length === 7) {
              labelFunction = (v) => {
                let d = moment(this.props.source.graphData.sessionBeginsAt, 'YYYY-MM-DD HH');
                let x = d.add((v * 1), this.props.source.graphData.step);
                return x.format('dd');
              };
              tickCount = 7
            } else {
              labelFunction = (v) => {
                let d = moment(this.props.source.graphData.sessionBeginsAt, 'YYYY-MM-DD HH');
                let x = d.add((v * 1), this.props.source.graphData.step);
                return (v % 5 === 0) ? x.format('DD MMM') : null;
              };
              tickCount = 30
            }
          }
        return (
            <VictoryChart
                height={c.rem * 10}
                width={c.WIDTH}
                padding={{ top: 20, bottom: 30, left: 40, right: 40 }}>
                <VictoryAxis
                    crossAxis
                    tickCount={tickCount}
                    tickFormat={labelFunction}
                    style={{
                        axis: { stroke: 'rgba(255, 163, 50, 0)' },
                        tickLabels: {
                            padding: 7,
                            fontSize: 0.5 * c.rem
                        }
                    }} />
                <VictoryAxis
                    crossAxis
                    dependentAxis
                    tickFormat={(t) => {
                        if (t > 999) {
                            if (t > 999999) {
                                return t = t / 1000000 + 'м';
                            } else {
                                return t = t / 1000 + 'к';
                            }
                        } else {
                            return t
                        }
                    }}
                    tickCount={4}
                    style={{
                        axis: { stroke: 'rgba(255, 163, 50, 0)' },
                        tickLabels: { padding: 15, fontSize: 0.5 * c.rem }
                    }}
                    domain={[0, maxLine]} />
                <VictoryLine
                    style={{
                        data: { stroke: c.ORANGE, strokeWidth: 1 },
                    }}
                    data={this.props.source.graphData.points[0]}
                    x={'x'} />
                <VictoryArea
                    style={{ data: { fill: 'rgba(255, 163, 50, 0.1)' } }}
                    data={this.props.source.graphData.points[0]}
                />
            </VictoryChart>
        )
    }
}