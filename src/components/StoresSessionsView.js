import React, { Component } from 'react';
import Splash from './Splash';
import {
  toNumberFormat,
  toNumberFormatReciept,
  getInterval,
  getPercentage,
  getReceiptsCount,
  getAverage
} from '../lib/functions';
import EStyleSheet from 'react-native-extended-stylesheet';
import moment from 'moment';
import VictoryChartView from './VictoryChartView';
import Drawer from 'react-native-drawer';
import Menu from './MenuView';
import ParamsModal from './ParamsModal';
import ModalView from './Modal';
import Header from './Header';
import { Actions } from 'react-native-router-flux';
import c from '../lib/const';
import Estyles from '../lib/styles'
import {
  View,
  Text,
  TouchableOpacity,
  BackHandler,
  ScrollView,
  Image,
  ImageBackground,
  Modal,
  Platform,
  RefreshControl,
} from 'react-native';
import RNExitApp from 'react-native-exit-app';


export default class StoresSessionView extends Component {
  constructor(props) {
    super(props);
    this.backHandler = this.onBackButton.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backHandler);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
  }

  onBackButton() {
    if (this.props.company.menu == true) {
      this.props.closeMenu()
    } else if (this.props.company.listOfShop.length > 1) {
      this.props.getCompany(this.props.session.token, this.props.session.intervalState);
      this.props.getAllShopsGraphData(this.props.session.token, this.props.session.intervalState)
      Actions.company()
      return true;
    } else {
      console.log('exitApp')
      RNExitApp.exitApp();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.company.modalState === 'exit' && (this.props.company.modalState !== nextProps.company.modalState)) {
      this.props.openModal();
    }
    if (this.props.session.intervalState && (nextProps.session.intervalState !== this.props.session.intervalState)) {
      this.props.saveSession(nextProps.session);
      this.props.getStoresSessions(this.props.session.token, this.props.storesSessions.selectShop, nextProps.session.intervalState)
      this.props.closeModal();
    }
  }

  onRefresh() {
    this.props.getStoresSessions(this.props.session.token, this.props.storesSessions.selectShop, this.props.session.intervalState)
  }
  onDotsButton() {
    this.props.setModalState('dots');
    this.props.openModal();
  }

  renderItem(row) {
    console.log('row', row)
    if (row) {
      return row.map((item, i) => {
        if (item.openDate === '1970-01-01 03:00:00' && item.closeDate === '1970-01-01 03:00:00') {
          var openData = 'нет данных'
          var closeData = 'нет данных'
        } else if (item.closeDate === '1970-01-01 03:00:00') {
          var openData = moment(item.openDate).format('DD-MM-YYYY HH:mm')
          var closeData = 'открыта'
        } else if (item.openDate === '1970-01-01 03:00:00') {
          var openData = 'нет данных'
          var closeData = moment(item.closeDate).format('DD-MM-YYYY HH:mm')
        } else {
          var openData = moment(item.openDate).format('DD-MM-YYYY HH:mm')
          var closeData = moment(item.closeDate).format('DD-MM-YYYY HH:mm')
        }
        return <View key={i} style={styles.orangeShop}>
          <TouchableOpacity
            style={styles.shop}
            onPress={() => this.props.getShopChecks(this.props.session.token, this.props.storesSessions.selectShop, this.props.session.intervalState, item)} >
            <View style={styles.leftHalf}>
              <Text style={styles.textName}>Смена {item.sessionNumber}</Text>
              <Text style={styles.textAddress}>{openData} - {closeData}</Text>
            </View>
            <View style={styles.rightHalf}>
              <Text style={styles.textShopAmount}>{toNumberFormat(item.sum)}</Text>
              <Text style={styles.textCashAmount}>чеков {Number(item.receipts) + Number(item.paybacks)} </Text>
            </View>
          </TouchableOpacity>
        </View>
      })
    }
  }

  render() {
    var buttonRight =
      <TouchableOpacity style={Estyles.icon}
        onPress={() => this.onDotsButton()}>
        <Image source={require('../images/dots.png')}
          style={{ width: 1.15 * c.rem, height: 0.8 * c.rem }} />
      </TouchableOpacity>

    var buttonRefresh =
    <TouchableOpacity style={Estyles.icon}
      onPress={() =>{this.props.getCompanyUpdate(this.props.session.token); this.onRefresh();} }>
      <Image source={require('../images/circular-arrow.png')}
        style={{ width: 1 * c.rem, height: 1.1 * c.rem }} />
    </TouchableOpacity>

    if (this.props.company.listOfShop && this.props.company.listOfShop.length > 1) {
      var buttonBack =
        <TouchableOpacity style={Estyles.icon}
          hitSlop={{ bottom: 0, top: 0, right: 1.5 * c.rem, left: 2 }}
          onPress={() => this.onBackButton()} >
          <Image source={require('../images/back.png')}
            style={{ width: 0.7 * c.rem, height: 1.19 * c.rem }} />
        </TouchableOpacity>
    } else {
      buttonBack =
        <TouchableOpacity style={Estyles.icon}
          hitSlop={{ bottom: 0, top: 0, right: 1.5 * c.rem, left: 2 }}
          onPress={() => this.props.openMenu()}>
          <Image source={require('../images/menu.png')}
            style={{ width: 1.2 * c.rem, height: c.rem }} />
        </TouchableOpacity>
    }
    if (this.props.company.modalState == 'exit') {
      var modalText =
        <Text style={styles.modalText}>Вы уверены что хотите выйти из учетной записи?</Text>
      var modalButton =
        <TouchableOpacity style={styles.activeButton} onPress={() => this.props.onLogout()}>
          <Text style={styles.activeText} >Выйти</Text>
        </TouchableOpacity>
      var modal =
        <ModalView
          {...this.props}
          modalText={modalText}
          modalButton={modalButton} />
    } else {
      modal =
        <ParamsModal {...this.props} />
    }
    if (this.props.storesSessions.isLoading == false) {
      if (this.props.storesSessions.sessions.length == 0 || !this.props.storesSessions.graphData) {
        if (this.props.storesSessions.error) {
          var errorContainer = <Text style={styles.textNone}>{this.props.storesSessions.error}</Text>
        }

        var viewChecks =
          <Drawer
            open={this.props.company.menu}
            type='overlay'
            content={<Menu {...this.props} />}
            tapToClose={false}
            openDrawerOffset={0.2} // 20% gap on the right side of drawer
            panCloseMask={0.1}
            tweenDuration={300}
            ref={(ref) => this._drawer}
            onOpenStart={() => {
              console.log('onopen');
              this.props.openMenu();
            }}
            onCloseStart={() => {
              console.log('onclose');
              this.props.closeMenu();
            }}
            negotiatePan
            captureGestures
            panOpenMask={0.1}>
            {modal}
            <Header
              buttonBack={buttonBack}
              headerText={this.props.storesSessions.selectShop.name}
              intervalText={getInterval(this.props.session.intervalState, 'Выручка')}
              buttonRight={buttonRight} />
            <ImageBackground source={require('../images/authBack.png')} style={{ width: c.WIDTH, height: c.HEIGHT }}>
              <Text style={styles.textNone}>В магазине отсутствуют смены</Text>
              {errorContainer}
            </ImageBackground>
          </Drawer>
      } else if (this.props.storesSessions.sessions.length !== 0 && this.props.storesSessions.graphData && this.props.storesSessions.graphData.points && this.props.storesSessions.graphData.points[0]) {

        if (this.props.storesSessions.shopData && this.props.storesSessions.shopData.inCompareWithLastInterval) {
          var datas = this.props.storesSessions.shopData.inCompareWithLastInterval
          var percentage = getPercentage(datas)
          var receiptsCount = getReceiptsCount(datas)
          var average = getAverage(datas)
        }
        viewChecks =
          <Drawer
            open={this.props.company.menu}
            type='overlay'
            content={<Menu {...this.props} />}
            tapToClose={false}
            openDrawerOffset={0.2} // 20% gap on the right side of drawer
            panCloseMask={0.1}
            tweenDuration={300}
            ref={(ref) => this._drawer}
            onOpenStart={() => {
              console.log('onopen');
              this.props.openMenu();
            }}
            onCloseStart={() => {
              console.log('onclose');
              this.props.closeMenu();
            }}
            negotiatePan
            captureGestures
            panOpenMask={0.1}>
            {modal}
            <Header
              buttonBack={buttonBack}
              headerText={this.props.storesSessions.selectShop.name}
              intervalText={getInterval(this.props.session.intervalState, 'Выручка')}
              isShowing = {true}
              buttonRefresh={buttonRefresh}
              buttonRight={buttonRight} />
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.props.storesSessions.isLoading}
                  onRefresh={this.onRefresh.bind(this)}
                  colors={[c.DEEPBLUE]}
                  progressBackgroundColor={c.SILVER}
                />}>
              <View style={Estyles.shopListContainer}>
                <View style={Estyles.allShopsContainer}>
                  <View style={styles.allShops}>
                    <View style={styles.headerRow}>
                      <View style={styles.numberContainer}>
                        <Text style={styles.textHeaderAmount}>{this.props.storesSessions.graphData && this.props.storesSessions.graphData.total
                          ? toNumberFormat(this.props.storesSessions.graphData.total) : 'Нет данных'}
                        </Text>
                        {percentage}
                      </View>
                      <VictoryChartView {...this.props} source={this.props.storesSessions} />
                      <View style={styles.row}>
                        <View style={styles.column}>
                          <View style={styles.numberContainer}>
                            <Text style={styles.textAmount}>{this.props.storesSessions.shopData && this.props.storesSessions.shopData.receiptsCount
                              ? toNumberFormatReciept(this.props.storesSessions.shopData.receiptsCount) : '--'}</Text>
                            {receiptsCount}
                          </View>
                          <Text style={styles.textLabel}>Чеков</Text>
                        </View>
                        <View style={styles.column}>
                          <View style={styles.numberContainer}>
                            <Text style={styles.textAmount}>{this.props.storesSessions.shopData && this.props.storesSessions.shopData.average
                              ? toNumberFormat(this.props.storesSessions.shopData.average) : '--'}</Text>
                            {average}
                          </View>
                          <Text style={styles.textLabel}>Средний чек</Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                {this.renderItem(this.props.storesSessions.sessions)}
              </View>
            </ScrollView>
          </Drawer>
      }
    } else {
      viewChecks =
        <View>
          <Splash />
        </View>
    }
    return (
      <View style={{ flex: 1 }}>
        {viewChecks}
      </View>
    )
  }
}

var styles = EStyleSheet.create({

  shop: {
    height: c.HEIGHT / 11,
    flexDirection: 'row',
    padding: '0.8rem',
    backgroundColor: c.WHITE,
    borderRadius: '0.3rem'
  },
  orangeShop: {
    paddingTop: '0.1rem',
    marginTop: '0.2rem',
    marginBottom: '0.5rem',
    backgroundColor: c.ORANGE,
    borderRadius: '0.3rem'
  },
  allShops: {
    flex: 1,
    justifyContent: 'space-around'
  },
  leftHalf: {
    justifyContent: 'space-between',
    flex: 5,
    alignItems: 'flex-start'
  },
  rightHalf: {
    flex: 2,
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  textName: {
    fontFamily: c.UbuntuR,
    fontSize: '0.6rem',
    color: c.BLACK
  },
  textNone: {
    fontFamily: c.UbuntuR,
    fontSize: '1rem',
    color: c.BLACK
  },
  textAmount: {
    fontFamily: c.RubikR,
    fontSize: '0.8rem',
    color: c.BLACK,
    marginRight: '0.3rem'
  },
  textShopAmount: {
    fontFamily: c.RubikR,
    fontSize: '0.6rem',
    color: c.BLACK,
    marginBottom: '0.3rem'
  },
  textCashAmount: {
    fontFamily: c.RubikR,
    fontSize: '0.5rem',
    color: c.SILVERm
  },
  textAddress: {
    fontFamily: c.UbuntuR,
    fontSize: '0.5rem',
    color: c.SILVER
  },
  headerRow: {
    flex: 1,
    flexDirection: 'column',
    paddingRight: 'rem',
    paddingTop: '0.5rem',
    justifyContent: 'space-around'
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingLeft: '1.5rem',
    paddingRight: 'rem',
    justifyContent: 'space-around'
  },
  column: {
    flex: 2,
    justifyContent: 'space-around',
    paddingLeft: '2.5rem'
  },
  textLabel: {
    fontFamily: c.UbuntuR,
    fontSize: '0.5rem',
    color: c.SILVER
  },
  textHeaderAmount: {
    fontFamily: c.RubikR,
    fontSize: '1.2rem',
    color: c.BLACK,
    marginLeft: '1rem',
  },
  underlay: {
    flex: 1,
    backgroundColor: 'rgba(31,97,201,0.5)'
  },
  activeText: {
    textAlign: 'center',
    fontFamily: c.UbuntuL,
    fontSize: '0.7rem',
    color: c.WHITE,
    marginBottom: '0.3rem'
  },
  activeButton: {
    paddingTop: '0.3rem',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: c.BLUE,
    borderRadius: '0.5rem',
    height: '2rem',
    width: '10rem'
  },
  modalText: {
    textAlign: 'center',
    fontFamily: c.UbuntuL,
    fontSize: 'rem',
    color: c.DGREY,
    marginBottom: '0.3rem'
  },
  numberContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
});
