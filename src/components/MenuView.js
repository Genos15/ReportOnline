import React, { Component } from 'react';
import {
  Platform,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  ImageBackground,
  Modal,
  BackHandler,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Actions } from 'react-native-router-flux';
import c from '../lib/const';


export default class Menu extends Component {
  constructor(props) {
    super(props);

  }
  onExitButton() {
    this.props.closeModal();
    this.props.setModalState('exit');
  }
  render() {
    return (
      <View style={styles.menuContainer}>
        <View style={styles.header}>
          <TouchableOpacity style={styles.closeMenu}
            onPress={() => this.props.closeMenu()}>
            <Image source={require('../images/close.png')}
              style={{ width: 1 * c.rem, height: 1 * c.rem }} />
          </TouchableOpacity>
          <Text style={styles.headerText}>Меню</Text>
        </View>
        <View style={styles.menuView}>
          <TouchableOpacity style={styles.menuItems}
          hitSlop={{ bottom: 1, top: 1, right: 1 * c.rem, left:1 }}
            onPress={() => {
              this.props.closeMenu();
              this.props.getCompany(this.props.session.token, this.props.session.intervalState);
              this.props.getAllShopsGraphData(this.props.session.token, this.props.session.intervalState)
              Actions.company()
            }}>
            <Image source={require('../images/shop.png')}
              style={{ width: 1 * c.rem, height: 1 * c.rem, marginRight: c.rem }} />
            <Text style={styles.itemsText}>Магазины</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.menuView}>
          <TouchableOpacity style={styles.menuItems}
          hitSlop={{ bottom: 1, top: 1, right: 1 * c.rem, left:1 }}
            onPress={() => { this.props.closeMenu(); this.props.getDocuments(this.props.session.token, this.props.session.intervalDocumentsState); Actions.documents() }}>
            <Image source={require('../images/receipt.png')}
              style={{ width: 1 * c.rem, height: 1 * c.rem , marginRight: c.rem}} />
            <Text style={styles.itemsText}>Документы</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.menuView}>
          <TouchableOpacity style={styles.menuItems}
          hitSlop={{ bottom: 1, top: 1, right: 1 * c.rem, left:1 }}
            onPress={() => this.onExitButton()}>
            <Image source={require('../images/logout.png')}
              style={{ width: 1 * c.rem, height: 1.07 * c.rem , marginRight: c.rem}} />
            <Text style={styles.itemsText}>Выйти</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

var styles = EStyleSheet.create({
  menuContainer: {
    backgroundColor: c.WHITE,
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    height: c.HEIGHT / 7,
    borderBottomWidth: 1,
    borderColor: c.TGREY,
    alignItems: 'center'
  },
  headerText: {
    fontSize: '1rem',
    color: c.BLACK,
    fontFamily: c.UbuntuL,
    paddingLeft: 'rem'
  },
  icon: {
    height: '2rem',
    width: '2rem',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '1rem'
  },
  closeMenu: {
    height: 2 * c.rem,
    width: 2 * c.rem,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '1rem',
    marginLeft: '1rem'
  },
  menuView: {
    padding: '1rem',
    paddingHorizontal: '1.5rem'
  },
  menuItems: {
    flexDirection: 'row',
    paddingTop: '1rem',
    height: '3rem',
    alignItems: 'center'
  },
  itemsText: {
    fontSize: '0.8rem',
    color: c.BLACK,
    fontFamily: c.UbuntuR
  },

})