import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Image,
    Modal,
    BackHandler,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Actions } from 'react-native-router-flux';
import c from '../lib/const';

export default class ModalView extends Component {
    constructor(props) {
        super(props);
    }
    chooseSection(interval) {
        if (this.props.documents.documentsRequest) {
            this.props.setDocumentsIntervalState(interval)
            this.props.closeModal();
        } else {
            this.props.setIntervalState(interval);
            this.props.closeModal();
        }
    }
    render() {
        return (
            <Modal
                animationType={'fade'}
                transparent
                visible={this.props.company.modalVisible}
                onRequestClose={() => { this.props.closeModal() }}>
                <View style={styles.underlay}>
                    <View style={styles.modalContainer}>
                        <TouchableOpacity style={styles.modalSection}
                            onPress={() => this.chooseSection('today')}>
                            <Text style={styles.sectionText}>Сегодня</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.modalSection}
                            onPress={() => this.chooseSection('yesterday')}>
                            <Text style={styles.sectionText}>Вчера</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.modalSection}
                            onPress={() => this.chooseSection('week')}>
                            <Text style={styles.sectionText}>За 7 дней</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                            onPress={() => this.chooseSection('month')}>
                            <Text style={styles.sectionText}>За 30 дней</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.cancelModalContainer}>
                        <TouchableOpacity style={styles.bottomModalSection}
                            onPress={() => this.props.closeModal()}>
                            <Text style={styles.cancelSectionText}>Отмена</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }
}

var styles = EStyleSheet.create({
    underlay: {
        flex: 1,
        backgroundColor: 'rgba(31,97,201,0.5)'
    },
    modalContainer: {
        flex: 5,
        marginTop: '15rem',
        marginBottom: '0.5rem',
        marginLeft: 'rem',
        marginRight: 'rem',
        backgroundColor: c.WHITE,
        borderRadius: '0.3rem'
    },
    modalSection: {
        flex: 1,
        borderBottomWidth: 1,
        borderColor: c.LGREY,
        justifyContent: 'center',
        alignItems: 'center'
    },
    sectionText: {
        fontFamily: c.UbuntuL,
        fontSize: '0.8rem',
        color: c.BLACK
    },
    cancelModalContainer: {
        flex: 1,
        height: '3rem',
        marginBottom: '1rem',
        marginLeft: 'rem',
        marginRight: 'rem',
        backgroundColor: c.WHITE,
        borderRadius: '0.3rem'
    },
    bottomModalSection: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cancelSectionText: {
        fontFamily: c.UbuntuR,
        fontSize: '0.8rem',
        color: c.BLACK
    },
})