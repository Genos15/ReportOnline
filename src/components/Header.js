import React, { Component } from 'react';
import {
    Platform,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    ImageBackground,
    Modal,
    BackHandler,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Actions } from 'react-native-router-flux';
import c from '../lib/const';


export default class Header extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        console.log('headerrrrr');
        if(this.props.isShowing) {
            return (
                <View style={styles.header}>
                    <View style={[styles.sideView, {marginLeft: 0.2 * c.rem}]}>
                        {this.props.buttonBack}
                    </View>
                    <View style={styles.title}>
                        <Text style={styles.headerText}>{this.props.headerText}</Text>
                        <Text style={styles.intervalText}>{this.props.intervalText}</Text>
                    </View>
                    <View style={styles.sideView}>
                        {this.props.buttonRefresh}
                        <Text style={styles.subti}>Обмен</Text>
                    </View>
    
                    <View style={styles.sideView}>
                        {this.props.buttonRight}
                        <Text style={styles.subti}>Период</Text>
                    </View>
                </View>
            );
        }else {
            return (
                <View style={styles.header}>
                    <View style={[styles.sideView, {marginLeft: 0.2 * c.rem}]}>
                        {this.props.buttonBack}
                    </View>
                    <View style={styles.title}>
                        <Text style={styles.headerText}>{this.props.headerText}</Text>
                        <Text style={styles.intervalText}>{this.props.intervalText}</Text>
                    </View>
                    {/* <View style={styles.sideView}>
                        {this.props.buttonRefresh}
                        <Text style={styles.subti}>Обмен</Text>
                    </View>
    
                    <View style={styles.sideView}>
                        {this.props.buttonRight}
                        <Text style={styles.subti}>Период</Text>
                    </View> */}
                </View>
            );
        }
        
    }
}

var styles = EStyleSheet.create({
    header: {
        paddingTop: Platform.OS === 'ios' ? '1rem' : 0,
        height: c.HEIGHT / 7,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: c.DEEPBLUE,
        paddingHorizontal: '0.5rem'
    },
    title: {
        flex: 6,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerText: {
        fontFamily: c.UbuntuR,
        fontSize: '0.9rem',
        color: c.WHITE,
    },
    intervalText: {
        fontFamily: c.UbuntuR,
        fontSize: '0.55rem',
        color: c.BLUE,
        marginTop: '0.5rem'
    },
    sideView: {
        marginTop: '0rem',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    subti: {
        fontFamily: c.UbuntuR,
        fontSize: '0.4rem',
        color: c.WHITE
    }
})