import React, { Component } from 'react';
import Splash from './Splash';
import { toNumberFormat} from '../lib/functions';
import EStyleSheet from 'react-native-extended-stylesheet';
import moment from 'moment';
import Drawer from 'react-native-drawer';
import Menu from './MenuView';
import Header from './Header';
import { Actions } from 'react-native-router-flux';
import ModalView from './Modal';
import c from '../lib/const';
import TabNavigator from 'react-native-tab-navigator';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import {
  View,
  Text,
  TouchableOpacity,
  BackHandler,
  ScrollView,
  Image,
  ImageBackground,
  Modal,
  Platform
} from 'react-native';

var image
var imageC = <Image source={require('../images/coins.png')}
  style={{ width: 1 * c.rem, height: 1 * c.rem, marginLeft: c.rem }} />
var imageCard = <Image source={require('../images/card.png')}
  style={{ width: 1 * c.rem, height: 1 * c.rem, marginLeft: c.rem }} />


export default class WorkingShiftView extends Component {
  constructor(props) {
    super(props);
    this.backHandler = this.onBackButton.bind(this);
  }

  componentDidMount() {
    console.log('didmount')
    BackHandler.addEventListener('hardwareBackPress', this.backHandler);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.company.modalState === 'exit' && (this.props.company.modalState !== nextProps.company.modalState)) {
      this.props.openModal();
    }
  }
  onBackButton() {
    Actions.storesSessions()
    return true;
  }

  getShiftState(data) {
    if (data.openDate === '1970-01-01 03:00:00' && data.closeDate === '1970-01-01 03:00:00') {
      return <View>
        <Text style={styles.generalInfo}>Статус: нет данных по открытию/закрытию смены</Text>
        <Text style={styles.generalInfo}>Открытие: -</Text>
        <Text style={styles.generalInfo}>Закрытие: -</Text>
      </View>
    } else if (data.closeDate === '1970-01-01 03:00:00') {
      return <View>
        <Text style={styles.generalInfo}>Статус: смена открыта</Text>
        <Text style={styles.generalInfo}>Открытие: {moment(data.openDate).format('DD-MM-YYYY HH:mm')}</Text>
        <Text style={styles.generalInfo}>Закрытие: -</Text>
      </View>
    } else if (data.openDate == '1970-01-01 03:00:00') {
      return <View>
        <Text style={styles.generalInfo}>Статус: нет данных по открытию смены</Text>
        <Text style={styles.generalInfo}>Открытие: -</Text>
        <Text style={styles.generalInfo}>Закрытие: {moment(data.closeDate).format('DD-MM-YYYY HH:mm')}</Text>
      </View>
    } else {
      return <View>
        <Text style={styles.generalInfo}>Статус: смена закрыта</Text>
        <Text style={styles.generalInfo}>Открытие: {moment(data.openDate).format('DD-MM-YYYY HH:mm')}</Text>
        <Text style={styles.generalInfo}>Закрытие: {moment(data.closeDate).format('DD-MM-YYYY HH:mm')}</Text>
      </View>
    }
  }

  renderItem(row) {
    console.log('row', row)
    if (row.length == 0) {
      return <Text style={styles.textNone}>Нет чеков</Text>
    }
    if (row) {
      return row.map((item, i) => {
        if (item.paymentType === 'CARD') {
          var image =
            <Image source={require('../images/card.png')}
              style={{ width: 1 * c.rem, height: 1 * c.rem, marginLeft: c.rem }} />
        } else {
          image =
            <Image source={require('../images/coins.png')}
              style={{ width: 1 * c.rem, height: 1 * c.rem, marginLeft: c.rem }} />
        }
        if (item.closeResultSum < 0) {
          var checkNumber =
            <Text style={[styles.textName, { color: 'red' }]}>Чек возврата номер {item.number}</Text>
          var checkSum =
            <Text style={[styles.textShopAmount, { color: 'red' }]}>{toNumberFormat(-1 * item.closeResultSum)}</Text>
        } else {
          checkNumber = <Text style={styles.textName}>Чек номер {item.number}</Text>
          checkSum = <Text style={styles.textShopAmount}>{toNumberFormat(item.closeResultSum)}</Text>
        }
        return <View key={i} style={styles.orangeShop}>
          <TouchableOpacity
            style={styles.shop}
            onPress={() => this.props.getCheck(item)} >
            <View style={styles.leftHalf}>
              {checkNumber}
              <Text style={styles.textAddress}>{moment(item.date).format('DD-MM-YYYY HH:mm')}</Text>
            </View>
            {image}
            <View style={styles.rightHalf}>
              {checkSum}
              <Text style={styles.textCashAmount}> {item.items.length} позиц.</Text>
            </View>
          </TouchableOpacity>
        </View>
      })
    }
  }
  render() {
    console.log('array', this.props.shopChecks.checks)
    const tableHead = ['Товар', 'Цена', 'Кол-во', 'Сумма', 'Оплата'];
    const tableData = []
    if (this.props.shopChecks.checks) {
      this.props.shopChecks.checks.forEach((item, i) => {
        for (let y = 0; y < item.items.length; y++) {
          tableData.push([item.items[y].productName.substr(0, 50), item.items[y].price, item.items[y].quantity.toFixed(2), item.items[y].cost.toFixed(2), item.paymentType === 'CARD' ? imageCard : imageC])
        }
      })
    }

    var modalText =
      <Text style={styles.modalText}>Вы уверены что хотите выйти из учетной записи?</Text>
    var modalButton =
      <TouchableOpacity style={styles.activeButton} onPress={() => this.props.onLogout()}>
        <Text style={styles.activeText}>Выйти</Text>
      </TouchableOpacity>

    var buttonBack =
      <TouchableOpacity style={styles.icon}
        hitSlop={{ bottom: 0, top: 0, right: 1.5 * c.rem, left: 2 }}
        onPress={() => this.onBackButton()} >
        <Image source={require('../images/back.png')}
          style={{ width: 0.8 * c.rem, height: 1.3 * c.rem }} />
      </TouchableOpacity>

    if (this.props.shopChecks.isLoading == false) {
      var view =
        <Drawer
          open={this.props.company.menu}
          type='overlay'
          content={<Menu {...this.props} />}
          tapToClose={false}
          openDrawerOffset={0} // 20% gap on the right side of drawer
          panCloseMask={0.1}
          tweenDuration={300}
          ref={(ref) => this._drawer}
          onOpenStart={() => {
            console.log('onopen');
            this.props.openMenu();
          }}
          onCloseStart={() => {
            console.log('onclose');
            this.props.closeMenu();
          }}
          negotiatePan
          captureGestures
          panOpenMask={0.1}>
          <ModalView
            {...this.props}
            modalText={modalText}
            modalButton={modalButton} />
          <Header
            buttonBack={buttonBack}
            headerText={this.props.storesSessions.selectShop.name}
            intervalText={`Смена номер: ${this.props.shopChecks.selectSession.sessionNumber}`} />
          <View style={styles.shopListContainer}>
            <View style={styles.allShopsContainer}>
              {this.getShiftState(this.props.shopChecks.selectSession)}
              <Text style={styles.generalInfo}>Выручка {this.props.shopChecks.selectSession.sum} р</Text>
              <View style={styles.checksInfo}>
                <View style={styles.checksInfoSide}>
                  <Text style={styles.checksInfoItems}>Пробитые чеки: {this.props.shopChecks.selectSession.receipts}</Text>
                  <Text style={styles.checksInfoItems}>Сумма чеков: {this.props.shopChecks.selectSession.sum}</Text>
                  <Text style={styles.checksInfoItems}>Оплата наличными: {this.props.shopChecks.selectSession.sumCashSell}</Text>
                  <Text style={styles.checksInfoItems}>Оплата картой: {this.props.shopChecks.selectSession.sumCardSell}</Text>
                </View>
                <View style={styles.checksInfoSide}>
                  <Text style={styles.checksInfoItemsRed}>Возвраты: {this.props.shopChecks.selectSession.paybacks}</Text>
                  <Text style={styles.checksInfoItemsRed}>Сумма возвратов: {-1 * this.props.shopChecks.selectSession.paybacksSum}</Text>
                  <Text style={styles.checksInfoItemsRed}>Возврат наличными: {-1 * this.props.shopChecks.selectSession.sumCashPayback}</Text>
                  <Text style={styles.checksInfoItemsRed}>Возврат по карте: {-1 * this.props.shopChecks.selectSession.sumCardPayback}</Text>
                </View>
              </View>
              <Text style={styles.generalInfo}>Итого по смене: {this.props.shopChecks.selectSession.total} р</Text>

            </View>
            <TabNavigator style={{ marginBottom: 1 * c.rem, height: c.HEIGHT - c.HEIGHT / 7 - c.HEIGHT / 2.8 }}
              tabBarStyle={{ marginBottom: c.HEIGHT - c.HEIGHT / 7 - (c.HEIGHT - (c.HEIGHT / 7 + c.HEIGHT / 2.75)) }}
              sceneStyle={{ marginTop: 2.1 * c.rem, paddingBottom: 0 }}>
              <TabNavigator.Item
                selected={this.props.shopChecks.selectTab === 'home'}
                title="Чеки"
                titleStyle={styles.titleStyle}
                selectedTitleStyle={styles.selectedTitleStyle}
                tabStyle={styles.tabStyle}
                onPress={() => this.props.setSelectTab('home')}>
                <ScrollView>
                  {this.renderItem(this.props.shopChecks.checks)}
                </ScrollView>
              </TabNavigator.Item>
              <TabNavigator.Item
                selected={this.props.shopChecks.selectTab === 'profile'}
                title="Розничные продажи"
                titleStyle={styles.titleStyle}
                selectedTitleStyle={[styles.selectedTitleStyle, { paddingHorizontal: c.rem }]}
                allowFontScaling={true}
                tabStyle={styles.tabStyle}
                onPress={() => this.props.setSelectTab('profile')}>
                <ScrollView>
                  <Table borderStyle={{ borderWidth: 0, borderColor: c.WHITE }} style={styles.table}>
                    <Row data={tableHead} flexArr={[3, 1, 1, 1]} style={styles.head} textStyle={styles.text} />
                    {
                      tableData.map((data, i) => {
                        console.log('data', data)
                        if (data[2] < 0) {
                          return <Row key={i} data={data} borderStyle={{ borderWidth: 0, borderColor: c.WHITE }} style={[styles.list, i % 2 && { backgroundColor: 'rgba(255, 163, 50, 0.1)' }]} flexArr={[3, 1, 1, 1, 1]} textStyle={[styles.text, { color: 'red' }]} />
                        } else {
                          return <Row key={i} data={data} borderStyle={{ borderWidth: 0, borderColor: c.WHITE }} style={[styles.list, i % 2 && { backgroundColor: 'rgba(255, 163, 50, 0.1)', alignItems: 'center' }]} flexArr={[3, 1, 1, 1, 1]} textStyle={styles.text} />
                        }
                      })
                    }
                  </Table>
                </ScrollView>
              </TabNavigator.Item>
            </TabNavigator>
          </View>
        </Drawer>
    } else {
      view =
        <View>
          <Splash />
        </View>
    }
    return (
      <View style={{ flex: 1 }}>
        {view}
      </View>
    )
  }
}

var styles = EStyleSheet.create({
  modalText: {
    textAlign: 'center',
    fontFamily: c.UbuntuL,
    fontSize: 'rem',
    color: c.DGREY,
    marginBottom: '0.3rem'
  },
  activeButton: {
    paddingTop: '0.3rem',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: c.BLUE,
    borderRadius: '0.5rem',
    height: '2rem',
    width: '10rem'
  },
  activeText: {
    textAlign: 'center',
    fontFamily: c.UbuntuL,
    fontSize: '0.7rem',
    color: c.WHITE,
    marginBottom: '0.3rem'
  },
  icon: {
    height: '3rem',
    justifyContent: 'center',
    alignItems: 'center',
    width: '3rem',
    paddingLeft: '0.5rem'
  },
  allShopsContainer: {
    height: c.HEIGHT / 2.8,
    paddingTop: '0.5rem',
    backgroundColor: c.WHITE
  },
  shopListContainer: {
    paddingBottom: '1rem',
    paddingLeft: '0.5rem',
    paddingRight: '0.5rem',
    backgroundColor: c.LGREY,
    height: c.HEIGHT - c.HEIGHT / 7
  },
  generalInfo: {
    fontFamily: c.RubikR,
    fontSize: '0.6rem',
    color: c.BLACK,
    marginLeft: '0.5rem',
    marginBottom: '0.3rem'
  },
  checksInfo: {
    flexDirection: 'row',
    marginTop: '0.1rem',
    marginHorizontal: '0.5rem'
  },
  checksInfoSide: {
    flex: 1
  },
  checksInfoItems: {
    fontFamily: c.RubikR,
    fontSize: '0.5rem',
    color: 'green',
    marginBottom: '0.5rem'
  },
  checksInfoItemsRed: {
    fontFamily: c.RubikR,
    fontSize: '0.5rem',
    color: 'red',
    marginBottom: '0.5rem'
  },
  containe: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: c.WHITE
  },
  titleStyle: {
    fontFamily: c.RubikR,
    fontSize: '0.7rem',
    color: c.BLACK,
  },
  selectedTitleStyle: {
    fontFamily: c.RubikR,
    fontSize: '0.7rem',
    color: c.BLACK,
    paddingHorizontal: 2 * c.rem,
    paddingBottom: 0.35 * c.rem,
    paddingTop: 0.6 * c.rem,
    borderColor: c.ORANGE,
    borderRadius: 0.3 * c.rem,
    borderWidth: 2
  },
  shop: {
    height: c.HEIGHT / 11,
    flexDirection: 'row',
    padding: '0.8rem',
    backgroundColor: c.WHITE,
    borderRadius: '0.3rem'
  },
  orangeShop: {
    paddingTop: '0.1rem',
    marginTop: '0.2rem',
    marginBottom: '0.5rem',
    backgroundColor: c.ORANGE,
    borderRadius: '0.3rem'
  },
  leftHalf: {
    justifyContent: 'space-between',
    flex: 5,
    alignItems: 'flex-start'
  },
  rightHalf: {
    flex: 2,
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  textName: {
    fontFamily: c.UbuntuR,
    fontSize: '0.6rem',
    color: c.BLACK
  },
  textShopAmount: {
    fontFamily: c.RubikR,
    fontSize: '0.6rem',
    color: c.BLACK,
    marginBottom: '0.3rem'
  },
  textCashAmount: {
    fontFamily: c.RubikR,
    fontSize: '0.5rem',
    color: c.SILVERm
  },
  textAddress: {
    fontFamily: c.UbuntuR,
    fontSize: '0.5rem',
    color: c.SILVER
  },
  head: {
    height: 40,
    backgroundColor: 'rgba(255, 163, 50, 0.1)'
  },
  text: {
    margin: 5,
    fontFamily: c.UbuntuR,
    fontSize: '0.5rem',
    color: c.BLACK
  },
  table: {
    backgroundColor: c.WHITE
  },
  textNone: {
    fontFamily: c.UbuntuR,
    fontSize: '1rem',
    color: c.BLACK,
    marginLeft: '1rem',
    marginTop: '1rem'
  },
})