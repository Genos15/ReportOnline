import React, { Component } from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import { toNumberFormat, getInterval, } from '../lib/functions';
import moment from 'moment';
import Drawer from 'react-native-drawer';
import Splash from './Splash';
import Menu from './MenuView';
import c from '../lib/const';
import { Actions } from 'react-native-router-flux';
import ModalView from './Modal';
import Header from './Header';
import Estyles from '../lib/styles';
import ParamsModal from './ParamsModal';
import TabNavigator from 'react-native-tab-navigator';
import RNExitApp from 'react-native-exit-app';
import {
    View,
    Text,
    TouchableOpacity,
    BackHandler,
    ScrollView,
    StyleSheet,
    Image,
    Modal,
    FlatList,
    ImageBackground,
    Platform
} from 'react-native';
var array

export default class DocumentsView extends Component {
    constructor(props) {
        super(props);
        this.backHandler = this.onBackButton.bind(this);
    }
    filterDocks(item) {
        var arrPaybacks = []
        var arrReceipts = []
        if (item) {
            item.forEach((item, i) => {
                if (item.closeResultSum < 0) {
                    arrPaybacks.push(item)
                } else {
                    arrReceipts.push(item)
                }
            })
        }
        this.props.addArrayChecks(arrPaybacks, arrReceipts)
    }
    

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.backHandler);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.session.token && (this.props.session.token !== nextProps.session.token)) {
            console.log('token')
            this.props.saveSession(nextProps.session);
            this.props.getDocuments(nextProps.session.token, nextProps.session.intervalDocumentsState);
          }
        if (nextProps.company.modalState === 'exit' && (this.props.company.modalState !== nextProps.company.modalState)) {
            this.props.openModal();
        }
        if (nextProps.documents.allChecks && (this.props.documents.allChecks !== nextProps.documents.allChecks)) {
            this.filterDocks(nextProps.documents.allChecks)
            console.log('fffffffffffffffffffffffffffffffffffnextProps')
        }
        if ((this.props.session.token === nextProps.session.token) && (nextProps.session.intervalDocumentsState !== this.props.session.intervalDocumentsState)) {
            console.log('intervalState')
            this.props.saveSession(nextProps.session);
            this.props.getDocuments(this.props.session.token, nextProps.session.intervalDocumentsState)
           
          }
          if (nextProps.documents.isLoading == false && (this.props.documents.isLoading !== nextProps.documents.isLoading)){
            this.props.saveSession(nextProps.session);
          }
    }
    onBackButton() {
        if (this.props.company.menu == true) {
            this.props.closeMenu()
        } else {
            console.log('exitApp')
            RNExitApp.exitApp();
        }
    }

    onDotsButton() {
        this.props.setModalState('dots');
        this.props.openModal();
    }

    renderItem(row) {
        console.log('row', row)
        var item = row.item
        if (item.paymentType === 'CARD') {
            var image =
                <Image source={require('../images/card.png')}
                    style={{ width: 1 * c.rem, height: 1 * c.rem, marginRight: c.rem }} />
        } else {
            image =
                <Image source={require('../images/coins.png')}
                    style={{ width: 1 * c.rem, height: 1 * c.rem, marginRight: c.rem }} />
        }
        if (item.closeResultSum < 0) {
            var checkNumber =
                <Text style={[styles.textName, { color: 'red' }]}>Чек возврата номер {item.number}</Text>
            var checkSum =
                <Text style={[styles.textShopAmount, { color: 'red' }]}>{toNumberFormat(-1 * item.closeResultSum)}</Text>
        } else {
            checkNumber = <Text style={styles.textName}>Чек номер {item.number}</Text>
            checkSum = <Text style={styles.textShopAmount}>{toNumberFormat(item.closeResultSum)}</Text>
        }

        return (<View style={styles.orangeShop}>
            <TouchableOpacity
                style={styles.shop}
                onPress={() => { this.props.fromDocuments(); this.props.getCheck(item) }} >
                <View style={styles.leftHalf}>
                    {checkNumber}
                    <Text style={styles.textAddress}>{moment(item.date).format('DD-MM-YYYY HH:mm')}</Text>
                </View>
                {image}
                <View style={styles.rightHalf}>
                    {checkSum}
                    <Text style={styles.textCashAmount}> {item.items.length} позиц.</Text>
                </View>
            </TouchableOpacity>
        </View>
        )
    }

    render() {

        console.log('fffffffffffffffffffffffffffffffffff', array)
        var buttonMenu =
            <TouchableOpacity style={Estyles.icon}
                hitSlop={{ bottom: 0, top: 0, right: 1.5 * c.rem, left: 2 }}
                onPress={() => this.props.openMenu()}>
                <Image source={require('../images/menu.png')}
                    style={{ width: 1.2 * c.rem, height: c.rem }} />
            </TouchableOpacity>
        var buttonRight =
            <TouchableOpacity style={Estyles.icon}
                onPress={() => this.onDotsButton()}>
                <Image source={require('../images/dots.png')}
                    style={{ width: 1.15 * c.rem, height: 0.8 * c.rem }} />
            </TouchableOpacity>

        var modalText =
            <Text style={styles.modalText}>Вы уверены что хотите выйти из учетной записи?</Text>
        var modalButton =
            <TouchableOpacity style={styles.activeButton} onPress={() => this.props.onLogout()}>
                <Text style={styles.activeText} >Выйти</Text>
            </TouchableOpacity>
        if (this.props.company.modalState !== 'exit') {
            var modal =
                <ParamsModal {...this.props} />
        } else {
            var modal =
                <ModalView
                    {...this.props}
                    modalText={modalText}
                    modalButton={modalButton} />
        }
        if (this.props.documents.isLoading) {
            console.log('splash')
            var view =
                <View>
                    <Splash />
                </View>
        } else {
            if (this.props.documents.error) {
                var view =
                    <Drawer
                        open={this.props.company.menu}
                        type='overlay'
                        content={<Menu {...this.props} />}
                        tapToClose={false}
                        openDrawerOffset={0.2} // 20% gap on the right side of drawer
                        panCloseMask={0.1}
                        tweenDuration={300}
                        ref={(ref) => this._drawer}
                        onOpenStart={() => {
                            console.log('onopen');
                            this.props.openMenu();
                        }}
                        onCloseStart={() => {
                            console.log('onclose');
                            this.props.closeMenu();
                        }}
                        negotiatePan
                        captureGestures
                        panOpenMask={0.1}>
                        <ModalView
                            {...this.props}
                            modalText={modalText}
                            modalButton={modalButton} />
                        <Header
                            buttonBack={buttonMenu}
                            headerText='Документы' />
                        <View style={Estyles.shopListContainer}>
                            <ImageBackground source={require('../images/authBack.png')} style={{ width: c.WIDTH, height: c.HEIGHT }}>
                                <Text style={styles.textNone}>В магазине отсутствуют смены</Text>
                                <Text style={styles.textNone}>{this.props.storesSessions.error}</Text>
                            </ImageBackground>
                        </View>
                    </Drawer>

            } else {
                view =
                    <Drawer
                        open={this.props.company.menu}
                        type='overlay'
                        content={<Menu {...this.props} />}
                        tapToClose={false}
                        openDrawerOffset={0.2} // 20% gap on the right side of drawer
                        panCloseMask={0.1}
                        tweenDuration={300}
                        ref={(ref) => this._drawer}
                        onOpenStart={() => {
                            console.log('onopen');
                            this.props.openMenu();
                        }}
                        onCloseStart={() => {
                            console.log('onclose');
                            this.props.closeMenu();
                        }}
                        negotiatePan
                        captureGestures
                        panOpenMask={0.1}>
                        {modal}
                        <Header
                            buttonBack={buttonMenu}
                            headerText='Документы'
                            intervalText={getInterval(this.props.session.intervalDocumentsState, 'Документы')}
                            buttonRight={buttonRight} />
                                <TabNavigator style={{ height: c.HEIGHT, paddingHorizontal: 0.5 * c.rem }}
                                    tabBarStyle={{ marginBottom: Platform.OS === 'ios' ? c.HEIGHT - c.HEIGHT / 4 + c.rem : c.HEIGHT - c.HEIGHT / 4, paddingHorizontal: 0.5 * c.rem }}
                                    sceneStyle={{ marginTop: c.HEIGHT / 4 - c.HEIGHT / 6, paddingBottom: 0, paddingHorizontal: 0.5 * c.rem }}>
                                    <TabNavigator.Item
                                        selected={this.props.shopChecks.selectTab === 'home'}
                                        title="Чеки"
                                        titleStyle={styles.titleStyle}
                                        selectedTitleStyle={styles.selectedTitleStyle}
                                        tabStyle={styles.tabStyle}
                                        onPress={() => this.props.setSelectTab('home')}>
                                        <View>
                                            <FlatList
                                                data={this.props.documents.receipts}
                                                renderItem={(item) => this.renderItem(item)}
                                                keyExtractor={item => item.documentUuid}
                                            />
                                        </View>
                                    </TabNavigator.Item>
                                    <TabNavigator.Item
                                        selected={this.props.shopChecks.selectTab === 'profile'}
                                        title="Возвраты"
                                        titleStyle={styles.titleStyle}
                                        selectedTitleStyle={[styles.selectedTitleStyle, { paddingHorizontal: c.rem }]}
                                        allowFontScaling={true}
                                        tabStyle={styles.tabStyle}
                                        onPress={() => this.props.setSelectTab('profile')}>
                                        <View>
                                            <FlatList
                                                data={this.props.documents.paybacks}
                                                renderItem={(item) => this.renderItem(item)}
                                                keyExtractor={item => item.documentUuid}
                                            />
                                        </View>
                                    </TabNavigator.Item>
                                </TabNavigator>

                    </Drawer>
                }
            }
            return (
            <View style={{ flex: 1 }}>
                            {view}
                        </View>

                        )
                    }
                }
                
var styles = EStyleSheet.create({
                            head: {
                            height: 40,
                        backgroundColor: 'rgba(255, 163, 50, 0.1)'
                    },
    text: {
                            margin: 5,
                        fontFamily: c.UbuntuR,
                        fontSize: '0.7rem',
                        color: c.BLACK
                    },
    table: {
                            backgroundColor: c.WHITE
                    },
    icon: {
                            height: '3rem',
                        justifyContent: 'center',
                        alignItems: 'center',
                    },
    shopListContainer: {

                            paddingLeft: '0.5rem',
                        paddingRight: '0.5rem',
                        backgroundColor: c.LGREY,
                    },
    shop: {
                            height: c.HEIGHT / 10,
                        flexDirection: 'row',
                        marginTop: '0.06rem',
                        padding: '0.8rem',
                        backgroundColor: c.WHITE,
                        borderRadius: '0.3rem'
                    },
    orangeShop: {
                            flex: 1,
                        paddingTop: '0.1rem',
                        marginTop: '0.2rem',
                        marginBottom: '0.5rem',
                        backgroundColor: c.ORANGE,
                        borderRadius: '0.3rem'
                    },
    leftHalf: {
                            justifyContent: 'space-between',
                        flex: 5,
                        alignItems: 'flex-start'
                    },
    rightHalf: {
                            flex: 2,
                        justifyContent: 'space-between',
                        alignItems: 'flex-start'
                    },
    textName: {
                            fontFamily: c.UbuntuR,
                        fontSize: '0.5rem',
                        color: c.BLACK
                    },
    textShopAmount: {
                            fontFamily: c.RubikR,
                        fontSize: '0.6rem',
                        color: c.BLACK,
                        marginBottom: '0.3rem'
                    },
    modalText: {
                            textAlign: 'center',
                        fontFamily: c.UbuntuL,
                        fontSize: 'rem',
                        color: c.DGREY,
                        marginBottom: '0.3rem'
                    },
    activeText: {
                            textAlign: 'center',
                        fontFamily: c.UbuntuL,
                        fontSize: '0.7rem',
                        color: c.WHITE,
                        marginBottom: '0.3rem'
                    },
    activeButton: {
                            paddingTop: '0.3rem',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: c.BLUE,
                        borderRadius: '0.5rem',
                        height: '2rem',
                        width: '10rem'
                    },
    tabStyle: {
                            justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: c.WHITE
                    },
    titleStyle: {
                            fontFamily: c.RubikR,
                        fontSize: '0.7rem',
                        color: c.BLACK,
                    },
    selectedTitleStyle: {
                            fontFamily: c.RubikR,
                        fontSize: '0.7rem',
                        color: c.BLACK,
                        paddingHorizontal: 2 * c.rem,
                        paddingBottom: 0.35 * c.rem,
                        paddingTop: 0.6 * c.rem,
                        borderColor: c.ORANGE,
                        borderRadius: 0.3 * c.rem,
                        borderWidth: 2
                    },
    textAddress: {
                            fontFamily: c.UbuntuR,
                        fontSize: '0.5rem',
                        color: c.SILVER
                    },
    textCashAmount: {
                            fontFamily: c.RubikR,
                        fontSize: '0.5rem',
                        color: c.SILVERm
                    },
    textNone: {
                            fontFamily: c.UbuntuR,
                        fontSize: '1rem',
                        color: c.BLACK
                    },
                });
