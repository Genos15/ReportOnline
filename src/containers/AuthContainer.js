import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import firebase from 'react-native-firebase';
import AuthView from '../components/AuthView';
import {ActionCreators} from '../actions/index'


const AuthViewWithSubscription = withSubscription(AuthView);

class AuthContainer extends Component {
  render () {
    return (
      <AuthViewWithSubscription {...this.props} />
    );
  }
}
function withSubscription (WrappedComponent) {
  return class extends React.Component {
    componentDidMount () {

      FCM = firebase.messaging();
      FCM.requestPermissions();
      FCM.getToken().then(token => {
        console.log('FCM token', token);
        this.props.setFCMToken(token)
      });
      /*firebase.getToken().then(token => {
        console.log('FCM token', token);
        //this.props.setFcmTokenState(token);
      });
      this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
        console.log(notif);  // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
        if (notif.local_notification) {
                // this is a local notification
        }
        if (notif.opened_from_tray) {
                // app is open/resumed because user clicked banner
        }
      });
      this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
        console.log('FCM token', token);
        this.props.setFcmTokenState(token);      // fcm token may not be available on first load, catch it here
      });*/
    }

    render () {
      console.log(firebase.SDK_VERSION)
      return <WrappedComponent {...this.props} />;
    }
  };
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps (state) {
  return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthContainer);
