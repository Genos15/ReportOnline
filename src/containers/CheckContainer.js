import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import CheckView from '../components/CheckView';
import {ActionCreators} from '../actions/index'


const CheckViewWithSubscription = withSubscription(CheckView);

class CheckContainer extends Component {
  render () {
    return (
      <CheckViewWithSubscription {...this.props} />
    );
  }
}
function withSubscription (WrappedComponent) {
  return class extends React.Component {
    render () {
      return <WrappedComponent {...this.props} />;
    }
  };
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps (state) {
  return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckContainer);