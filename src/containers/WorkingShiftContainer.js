import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import WorkingShiftView from '../components/WorkingShiftView';
import {ActionCreators} from '../actions/index'


const WorkingShiftViewWithSubscription = withSubscription(WorkingShiftView);

class WorkingShiftContainer extends Component {
  render () {
    return (
      <WorkingShiftViewWithSubscription {...this.props} />
    );
  }
}

function withSubscription (WrappedComponent) {
  return class extends React.Component {
    render () {
      return <WrappedComponent {...this.props} />;
    }
  };
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps (state) {
  return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkingShiftContainer);