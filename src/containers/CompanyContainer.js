import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CompanyView from '../components/CompanyView';
import {ActionCreators} from '../actions/index';

const CompanyViewWithSubscription = withSubscription(CompanyView);

class CompanyContainer extends Component {
  render () {
    return (
      <CompanyViewWithSubscription {...this.props} />
    );
  }
}

function withSubscription (WrappedComponent) {
  return class extends React.Component {
    render () {
      return <WrappedComponent {...this.props} />;
    }
  };
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps (state) {
  return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(CompanyContainer);