import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import StoresSessionsView from '../components/StoresSessionsView';
import {ActionCreators} from '../actions/index'


const StoresSessionsViewWithSubscription = withSubscription(StoresSessionsView);

class StoresSessionsContainer extends Component {
  render () {
    return (
      <StoresSessionsViewWithSubscription {...this.props} />
    );
  }
}

function withSubscription (WrappedComponent) {
  return class extends React.Component {
    render () {
      return <WrappedComponent {...this.props} />;
    }
  };
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps (state) {
  return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(StoresSessionsContainer);