import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DocumentsView from '../components/DocumentsView';
import {ActionCreators} from '../actions/index';

const DocumentsViewWithSubscription = withSubscription(DocumentsView);

class DocumentsContainer extends Component {
  render () {
    return (
      <DocumentsViewWithSubscription {...this.props} />
    );
  }
}

function withSubscription (WrappedComponent) {
  return class extends React.Component {
    render () {
      return <WrappedComponent {...this.props} />;
    }
  };
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps (state) {
  return state;
}

export default connect(mapStateToProps, mapDispatchToProps)(DocumentsContainer);