const defaultState = {
  allChecks: null,
    isLoading: false,
    fromDocuments: false,
    paybacks: null,
    receipts: null,
    documentsRequest: false
  };
  export default (state = defaultState, action) => {
    switch (action.type) {
      
      case 'DOCUMENTS_REQUEST':
        return Object.assign({}, state, {
          isLoading: true,
          allChecks: null,
          documentsRequest: true
         
        });
      case 'DOCUMENTS_SUCCESS':
        return Object.assign({}, state, {
          isLoading: false,
          error: null,
          allChecks: action.payload,
          fromDocuments: false
        });
      case 'DOCUMENTS_FAILURE':
        return Object.assign({}, state, {
          error: action.payload,
          isLoading: false,
          fromDocuments: false
        });
      
      case 'DELETE_SESSION_SUCCESS':
        return Object.assign({}, state, {
            allchecks: null,
            isLoading: false,
            fromDocuments: false
        });

      case 'FROM_DOCUMENTS':
      return Object.assign({}, state, {
         fromDocuments: true,
         
      });
      case 'GET_COMPANY_REQUEST':
      return Object.assign({}, state, {
        fromDocuments: false,
        documentsRequest: false
      })
      case 'ADD_ARRAY_CHECKS':
      return Object.assign({}, state, {
         paybacks: action.paybacks,
         receipts: action.receipts
         
      });
      default:
        return state;
    }
  };
  