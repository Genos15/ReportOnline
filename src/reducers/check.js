const defaultState = {
    selectCheck: []
   };
 
export default (state=defaultState, action)=>{
  switch (action.type){
    case 'OPEN_CHECK':
      return Object.assign({}, state, {
        selectCheck: action.payload
      });
      case 'DELETE_SESSION_SUCCESS':
      return Object.assign({}, state, {
        selectCheck: []
      });
    default: return state;
  }
}
         