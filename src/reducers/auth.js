const defaultState = {
  login: null,
  password: null,
  isLoading: false,
  error: null,
  isPasswordHidden: true,
  modalVisible: false,
  modalState: null
};
export default (state = defaultState, action) => {
  switch (action.type) {
    case 'TOGGLE_EYE':
      return Object.assign({}, state, {
        isPasswordHidden: !state.isPasswordHidden
      });
    case 'SET_LOGIN':
      return Object.assign({}, state, {
        login: action.payload
      });
    case 'SET_PASSWORD':
      return Object.assign({}, state, {
        password: action.payload
      });

    case 'LOGIN_REQUEST':
      return Object.assign({}, state, {
        isLoading: true
      });
    case 'LOGIN_SUCCESS':
      return Object.assign({}, state, {
        isLoading: false,
        error: null
      });
    case 'LOGIN_FAILURE':
      return Object.assign({}, state, {
        error: action.payload,
        isLoading: false
      });
    case 'OPEN_MODAL':
      return Object.assign({}, state, {
        modalVisible: true
      });
    case 'CLOSE_MODAL':
      return Object.assign({}, state, {
        modalVisible: false,
        modalState: null,
        error: null
      });
    case 'SET_MODAL_STATE':
      return Object.assign({}, state, {
        modalState: action.payload
      });
     
    case 'DELETE_SESSION_SUCCESS':
      return Object.assign({}, state, {
        isLoading: false,
        error: null,
        isPasswordHidden: true,
        modalVisible: false,
        modalState: null
      });
    default:
      return state;
  }
};
