const defaultState = {
  isLoading: null,
  error: null,
  shops: [],
  modalVisible: false,
  modalState: null,
  shopsClickHouse: null,
  listOfShop: null,
  isAdd: null,
  graphData: null,
  menu: false
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'GET_COMPANY_SUCCESS':
      return Object.assign({}, state, {
        shops: action.payload,
        isLoading: false
      });
    case 'GET_COMPANY_REQUEST':
      return Object.assign({}, state, {
        isLoading: true
      });
    case 'GET_COMPANY_FAILURE':
      return Object.assign({}, state, {
        error: action.payload,
        isLoading: false
      });
    case 'GET_SHOP_LIST_REQUEST':
      return Object.assign({}, state, {
        isLoading: true
      });
    case 'GET_SHOP_LIST_SUCCESS':
      return Object.assign({}, state, {
        shopsClickHouse: action.payload,
        isLoading: false
      });
    case 'GET_SHOP_LIST_FAILURE':
      return Object.assign({}, state, {
        error: action.payload,
        isLoading: false
      });
    case 'ADD_LIST_OF_SHOPS':
      return Object.assign({}, state, {
        listOfShop: action.payload,
        isAdd: true,
        modalVisible: false
      });
    case 'NOT_IS_ADD':
      return Object.assign({}, state, {
        isAdd: null
      });
    case 'GET_ALL_SHOPS_GRAPH_DATA_REQUEST':
      return Object.assign({}, state, {

      });
    case 'GET_ALL_SHOPS_GRAPH_DATA_SUCCESS':
      return Object.assign({}, state, {
        graphData: action.payload
      });
    case 'GET_ALL_SHOPS_GRAPH_DATA_FAILURE':
      return Object.assign({}, state, {

      });
    case 'OPEN_MODAL':
      return Object.assign({}, state, {
        modalVisible: true
      });
    case 'CLOSE_MODAL':
      return Object.assign({}, state, {
        modalVisible: false,
        modalState: null,
        error: null
      });
    case 'SET_MODAL_STATE':
      return Object.assign({}, state, {
        modalState: action.payload
      });
      case 'OPEN_MENU':
      return Object.assign({}, state, {
        menu: true
      });
    case 'CLOSE_MENU':
      return Object.assign({}, state, {
        menu: false
      });
    case 'DELETE_SESSION_SUCCESS':
      return Object.assign({}, state, {
        isLoading: null,
        error: null,
        shops: [],
        modalVisible: false,
        modalState: null,
        shopsClickHouse: null,
        listOfShop: null,
        isAdd: null,
        graphData: null,
        menu: false
      });
    case 'GET_COMPANY_UPDATE_SUCCESS':
      return Object.assign({}, state, {
        baseIsUpdate: true,
        isLoading: false
      });
    case 'GET_COMPANY_UPDATE_REQUEST':
      return Object.assign({}, state, {
        isLoading: true
      });
    case 'GET_COMPANY_UPDATE_FAILURE':
      return Object.assign({}, state, {
        error: action.payload,
        isLoading: false
      });
    default: return state;
  }
}
