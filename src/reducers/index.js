import { combineReducers } from 'redux';

import company from './company';
import shopChecks from './shopChecks';
import check from './check';
import auth from './auth';
import session from './session';
import storesSessions from './storesSessions';
import documents from './documents';


const mainReducer = combineReducers({
  company,
  shopChecks,
  check,
  auth,
  session,
  storesSessions,
  documents
});

export default mainReducer;