const defaultState = {
  isLoading: false,
  error: null,
  sessions: [],
  selectShop: [],
  graphData: null,
  shopData: null,
  intervalState: 'today'
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'GET_STORES_SESSIONS_REQUEST':
      return Object.assign({}, state, {
        isLoading: true
      });
    case 'GET_STORES_SESSIONS_SUCCESS':
      return Object.assign({}, state, {
        sessions: action.payload,
        isLoading: false,
        selectShop: action.item,
        error: null,
      });
    case 'GET_STORES_SESSIONS_FAILURE':
      return Object.assign({}, state, {
        error: action.payload,
        isLoading: false,
        selectShop: action.item,
        sessions: [],
      });
      case 'FROM_DOCUMENTS':
      return Object.assign({}, state, {
        selectShop: []
         
      });
    case 'GET_GRAPH_DATA_REQUEST':
      return Object.assign({}, state, {
        isLoading: true,
      });
    case 'GET_GRAPH_DATA_SUCCESS':
      return Object.assign({}, state, {
        isLoading: false,
        graphData: action.payload,
        error: null,
      });
    case 'GET_GRAPH_DATA_FAILURE':
      return Object.assign({}, state, {
        error: action.payload,
        isLoading: false,
        graphData: null,
      });
    case 'GET_SHOP_REQUEST':
      return Object.assign({}, state, {
        isLoading: true
      });
    case 'GET_SHOP_SUCCESS':
      return Object.assign({}, state, {
        isLoading: false,
        shopData: action.payload
      });
    case 'GET_SHOP_FAILURE':
      return Object.assign({}, state, {
        error: action.payload,
        isLoading: false
      });
    case 'SET_INTERVAL_STATE':
      return Object.assign({}, state, {
        intervalState: action.payload
      });
    case 'DELETE_SESSION_SUCCESS':
      return Object.assign({}, state, {
        isLoading: false,
        error: null,
        sessions: [],
        selectShop: [],
        graphData: null,
        shopData: null,
        intervalState: 'today'
      });
    default: return state;
  }
}