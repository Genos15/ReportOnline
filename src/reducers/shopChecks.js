const defaultState = {
  isLoading: false,
  error: null,
  checks: [],
  selectSession: null,
  selectTab: 'home'
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'GET_SHOP_CHECKS_REQUEST':
      return Object.assign({}, state, {
        isLoading: true
      });
    case 'GET_SHOP_CHECKS_SUCCESS':
      return Object.assign({}, state, {
        checks: action.payload,
        isLoading: false,
        selectSession: action.item
      });
    case 'GET_SHOP_CHECKS_FAILURE':
      return Object.assign({}, state, {
        error: action.payload,
        isLoading: false,
        selectSession: action.item
      });
    case 'SET_SELECT_TAB':
      return Object.assign({}, state, {
        selectTab: action.payload
      });
    case 'DELETE_SESSION_SUCCESS':
      return Object.assign({}, state, {
        isLoading: false,
        error: null,
        checks: [],
        selectTab: 'home'
      });
    default: return state;
  }
}