const defaultState = {
    token: null,
    intervalState: 'today',
    intervalDocumentsState: 'today',
    isSessionSet: false,
    FCMtoken: null,
    lastWindow: 'company'
  };
  
export default (state = defaultState, action) => {
  switch (action.type) {
    case 'SET_SESSION_TO_STATE':
      return Object.assign({}, state, {
        token: action.payload.token,
        intervalState: action.payload.intervalState,
        intervalDocumentsState: action.payload.intervalDocumentsState,
        isSessionSet: true,
        FCMtoken: action.payload.FCMtoken
      });
    case 'DELETE_SESSION_SUCCESS':
      return Object.assign({}, state, {
        token: null,
        isSessionSet: false,
        intervalState: 'today'
      });
      case 'SET_INTERVAL_STATE':
      return Object.assign({}, state, {
        intervalState: action.payload
      });
      case 'SET_FCM_TOKEN':
      return Object.assign({}, state, {
        FCMtoken: action.payload
      });
      case 'SET_DOCUMENTS_INTERVAL_STATE':
      return Object.assign({}, state, {
        intervalDocumentsState: action.payload
      });
      case 'GET_COMPANY_REQUEST':
      return Object.assign({}, state, {
        lastWindow: 'company'
      });
      case 'DOCUMENTS_REQUEST':
        return Object.assign({}, state, {
          lastWindow: 'documents'
        });
    case 'LOGIN_SUCCESS':
      return Object.assign({}, state, {
        token: action.payload
      });
    default:
      return state;
  }
};