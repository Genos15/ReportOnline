module.exports = {
    SESSION_TOKEN_KEY: 'SESSION_TOKEN_KEY',
    Hapi:{
        authUrl: 'https://lk-test.lad24.ru/apps/types/onlineReport',
        url: 'https://evotorproxy.lad24.ru:8181',
        urlClick: 'https://onlinereport.lad24.ru',
        urlUpdate: 'https://lk-test2.lad24.ru/apps/types/onlineReport/runFromApp/update',
    }
}
