export const errorCase = (error) => {
    if (error) {
      switch (error) {
        case 'Network Error':
          return 'ОТСУТСТВУЕТ СОЕДИНЕНИЕ';
        case 'noPhone':
          return 'Введите номер телефона';
        case 'timeout of 10000ms exceeded':
          return 'ОШИБКА СОЕДИНЕНИЯ';
        case 504:
          return 'Сервер не отвечает';
        case 'Location request timed out':
          return 'Не удалось определить местоположение';
       
        case 404:
        case 401:
          
        default:
          return 'Ошибка';
      }
    }
  };
  