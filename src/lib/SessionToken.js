import store from 'react-native-simple-store';
import CONFIG from './config';

export class SessionToken {

  constructor () {
    this.SESSION_TOKEN_KEY = CONFIG.SESSION_TOKEN_KEY;
    this.SESSION_TOKEN_KEY2 = CONFIG.SESSION_TOKEN_KEY2;
  }
  saveSession (state) {
    return store.save(this.SESSION_TOKEN_KEY, {
      session: state
    });
  }
  saveSessionUpdate(state) {
    return store.save(this.SESSION_TOKEN_KEY2, {
      session: state
    });
  }
  /**
   * ### getSessionToken
   * @param {Object} sessionToken the currentUser object
   *
   * When Hot Loading, the sessionToken  will be passed in, and if so,
   * it needs to be stored on the device.  Remember, the store is a
   * promise so, have to be careful.
   */
  getSession (token, intervalState, intervalDocumentsState, lastWindow) {
    console.log('get session');
    if (token && intervalState && intervalDocumentsState && FCMtoken && lastWindow) {
      console.log(token, intervalState, intervalDocumentsState, FCMToken, lastWindow)
      return store.save(this.SESSION_TOKEN_KEY, {
        token: token,
        intervalState: intervalState,
        intervalDocumentsState: intervalDocumentsState,
        FCMtoken: FCMtoken,
        lastWindow: lastWindow
      }).then(() => {
        return store.get(this.SESSION_TOKEN_KEY);
      });
    }
    return store.get(this.SESSION_TOKEN_KEY);
  }

  deleteSession () {
    console.log('delete session');
    return store.delete(this.SESSION_TOKEN_KEY);
  }


  getUpdateSession (token, intervalState, intervalDocumentsState, lastWindow) {
    console.log('get Update session');
    if (token && intervalState && intervalDocumentsState && FCMtoken && lastWindow) {
      console.log(token, intervalState, intervalDocumentsState, FCMToken, lastWindow)
      return store.save(this.SESSION_TOKEN_KEY, {
        token: token,
        intervalState: intervalState,
        intervalDocumentsState: intervalDocumentsState,
        FCMtoken: FCMtoken,
        lastWindow: lastWindow
      }).then(() => {
        return store.get(this.SESSION_TOKEN_KEY2);
      });
    }
    return store.get(this.SESSION_TOKEN_KEY2);
  }

  deleteUpdateSession () {
    console.log('delete update session');
    return store.delete(this.SESSION_TOKEN_KEY2);
  }

}

export let sessionToken = new SessionToken();
