import axios from 'axios';
import config from './config';
export default {
    async authorize (login, password) {
        try {
          let response = await axios({
            method: 'post',
            url: config.Hapi.authUrl + '/login',
            data: {
              login: login,
              password: password
            },
            timeout: 10000
          });
          console.log(response);
          return response;
        } catch (error) {
          console.log(error.message);
          if (error.message === 'Network Error' || error.message === 'timeout exceeded') {
            return error.message;
          } else if (error.response && error.response.status) {
            return error.response.status;
          }
        }
      },
    async getCompany (token) {
        try {
            let response = await axios({
                method: 'get',
                url: config.Hapi.url + '/stores',
                headers: {'Authorization': 'Bearer ' + token},
                timeout: 10000
            });
            console.log(response);
            return response;
        } catch (error) {
            console.log(error.message);
            if (error.message === 'Network Error' || error.message === 'timeout of 10000ms exceeded') {
                return error.message;
            } else if (error.response && error.response.status) {
                return error.response.status;
            }
        }
    },
    async getCompanyUpdate(token) {
      try{
        let response = await axios({
          method: 'post',
          url:config.Hapi.urlUpdate, 
          headers: {'Authorization':'Bearer ' + token},
          timeout: 10000
        });
        console.log(response);
        return response;
      }catch(error) {
          console.log('eeeror', error.message);
          if(error.message === 'Network Error' || error.message ==='timeout of 10000ms exceeded') {
            return error.message;
          }else if(error.response && error.response.status) {
            return error.response.status;
          }
      }
    },
    async getShopList (token, interval) {
        try {
          let response = await axios({
            method: 'get',
            url: config.Hapi.urlClick + '/shop',
            params: {
              interval: interval
            },
            headers: {'Authorization': token},
            timeout: 10000
          });
          console.log(response);
          return response;
        } catch (error) {
          
          if (error.message === 'Network Error' || error.message === 'timeout of 10000ms exceeded') {
            return error.message;                                     
          } else if (error.response && error.response.status) {
            return error.response.status;
          }
        }
      },
      async getAllShopsGraphData (token, interval) {
        try {
          let response = await axios({
            method: 'get',
            url: config.Hapi.urlClick + '/shop/graph',
            params: {
              interval: interval
            },
            headers: {'Authorization': token},
            timeout: 100000
          });
          console.log(response);
          return response; 
        } catch (error) {
          console.log(error.message);
          if (error.message === 'Network Error' || error.message === 'timeout of 10000ms exceeded') {
            return error.message;
          } else if (error.response && error.response.status) {
            return error.response.status;
          }
        }
      },
      async getGraphData (token, item, interval) {
        console.log('getGraphData', config.Hapi.urlClick + '/shop/' + item + '/graph', interval);
        try {
          let response = await axios({
            method: 'get',
            url: config.Hapi.urlClick + '/shop/' + item + '/graph',
            params: {
              interval: interval
            },
            headers: {'Authorization': token},
            timeout: 10000
          });
          console.log(response);
          return response;
        } catch (error) {
          console.log(error.message);
          if (error.message === 'Network Error' || error.message === 'timeout of 10000ms exceeded') {
            return error.message;
          } else if (error.response && error.response.status) {
            return error.response.status;
          }
        }
      },
    async getShopChecks (token, item, interval, sessionNumber){
        try {
            let response = await axios({
                method: 'get',
                url: config.Hapi.urlClick + '/receipt/' + item,
                params: {
                  interval: interval,
                  session: sessionNumber
                },
                headers: {'Authorization':  token},
                timeout: 10000
            });
            console.log('запрос', config.Hapi.urlClick + '/receipt/' + item)
            console.log(response);
           
            return response;
        } catch (error) {
            console.log(error.message);
            if (error.message === 'Network Error' || error.message === 'timeout of 10000ms exceeded') {
                return error.message;
            } else if (error.response && error.response.status) {
                return error.response.status;
               
            }
        }
    },

    async getDocuments (token, interval){
      try {
          let response = await axios({
              method: 'get',
              url: config.Hapi.urlClick + '/receipt/all',
              params: {
                interval: interval
              },
              headers: {'Authorization':  token},
              timeout: 10000
          });
          console.log('запрос', config.Hapi.urlClick + '/receipt/all')
          console.log(response);
         
          return response;
      } catch (error) {
          console.log(error.message);
          if (error.message === 'Network Error' || error.message === 'timeout of 10000ms exceeded') {
              return error.message;
          } else if (error.response && error.response.status) {
              return error.response.status;
             
          }
      }
  },
    async getStoresSessions (token, item, interval){
      try {
          let response = await axios({
              method: 'get',
              url: config.Hapi.urlClick + '/session/' + item,
              params: {
                interval: interval
              },
              headers: {'Authorization':  token},
             
              timeout: 10000
          });
          console.log('запрос', config.Hapi.urlClick + '/receipt/' + item)
          console.log(response);
         
          return response;
      } catch (error) {
          console.log(error.message);
          if (error.message === 'Network Error' || error.message === 'timeout of 10000ms exceeded') {
              return error.message;
          } else if (error.response && error.response.status) {
              return error.response.status;
             
          }
      }
  },
    async getShop (token, item, interval) {
      console.log(interval, item);
      console.log(config.Hapi.urlClick + '/shop/' + item + '/data');
      try {
        let response = await axios({
          method: 'get',
          url: config.Hapi.urlClick + '/shop/' + item + '/data',
          params: {
            interval: interval
          },
          headers: {'Authorization': token},
          timeout: 10000
        });
        console.log(response);
        return response;
      } catch (error) {
        console.log(error.message);
        if (error.message === 'Network Error' || error.message === 'timeout of 10000ms exceeded') {
          return error.message;
        } else if (error.response && error.response.status) {
          return error.response.status;
        }
      }
    }

}

