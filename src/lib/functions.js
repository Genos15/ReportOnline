import React from 'react';
import {
  View,
  Image,
  Text
} from 'react-native';
import c from './const';
import EStyleSheet from 'react-native-extended-stylesheet';


export const toNumberFormat = (number) =>{
    if (!number) {
      return 0 + ' ₽';
    } else if (~number.toString().indexOf('.')) {
      var str = number.toFixed(2);
      if (str.length > 6) {
        var formatStr = str.slice(0, str.length - 6) + ' ' + str.slice(-6).replace('.', ',');
        if (formatStr.length > 10) {
          var milFormatStr = formatStr.slice(0, formatStr.length - 10) + ' ' + formatStr.slice(-10).replace('.', ',');
          return milFormatStr + ' ₽';
        } else return formatStr + ' ₽';
      } else {
        return str.replace('.', ',') + ' ₽';
      }
    } else if (number.toString().length > 3) {
      str = number.toString();
      formatStr = str.slice(0, str.length - 3) + ' ' + str.slice(-3);
      if (formatStr.length > 7) {
        milFormatStr = formatStr.slice(0, formatStr.length - 7) + ' ' + formatStr.slice(-7);
        return milFormatStr + ' ₽';
      } else return formatStr + ' ₽';
    } else {
      return number + ' ₽';
    };
  }

  export const toNumberFormatReciept = (number)=>{
    if (!number) {
      return 0;
    } else if (number.toString().length > 3) {
      var str = number.toString();
      var formatStr = str.slice(0, str.length - 3) + ' ' + str.slice(-3);
      if (formatStr.length > 7) {
        var milFormatStr = formatStr.slice(0, formatStr.length - 7) + ' ' + formatStr.slice(-7);
        return milFormatStr;
      } else return formatStr;
    } else return number;
  };

  export const renderLargeArrow = (number) =>{
    if (number > 0) {
      return <Image source={require('../images/arrowUp.png')}
        style={{width: 0.42 * c.rem, height: 0.6 * c.rem, marginBottom: 0.15 * c.rem, marginRight: 0.2 * c.rem}} />;
    } else if (number < 0) {
      return <Image source={require('../images/arrowDown.png')}
        style={{width: 0.42 * c.rem, height: 0.6 * c.rem, marginBottom: 0.15 * c.rem, marginRight: 0.2 * c.rem}} />;
    } else {
      return <View />;
    }
  };

  export const renderArrow = (number) => {
    if (number > 0) {
      return <Image source={require('../images/arrowUp.png')}
        style={{width: 0.28 * c.rem, height: 0.4 * c.rem, marginBottom: 0.15 * c.rem, marginRight: 0.1 * c.rem}} />;
    } else if (number < 0) {
      return <Image source={require('../images/arrowDown.png')}
        style={{width: 0.28 * c.rem, height: 0.4 * c.rem, marginBottom: 0.15 * c.rem, marginRight: 0.1 * c.rem}} />;
    } else {
      return <View />;
    }
  };

  export const getInterval = (interval, data)=>{
    switch (interval) {
      case 'today':
        var screenDescription = data + ' за сегодня';
        break;
      case 'yesterday':
        screenDescription = data + ' за вчера';
        break;
      case 'week':
        screenDescription = data + ' за неделю';
        break;
      case 'month':
        screenDescription = data + ' за 30 дней';
        break;
      default: screenDescription = data + ' за сегодня';
    }
    return screenDescription
  }
  
  export const getPercentage = (datas)=>{
    return <View style={{ marginLeft: 1 * c.rem, flexDirection: 'row', alignItems: 'flex-end' }}>
            {renderLargeArrow(datas.total)}
            <Text style={datas.total < 0 ? styles.largeMinusPercentage
              : (datas.total === 0
                ? styles.largeNeutralPercentage : styles.largePlusPercentage)}>
              {datas.total
                || datas.total === 0
                ? Math.abs(datas.total).toString() + '%' : ''}
            </Text>
          </View>
  }
  export const getReceiptsCount = (datas)=>{
    return receiptsCount =
    <View style={{ marginLeft: 0.2 * c.rem, flexDirection: 'row', alignItems: 'flex-end' }}>
      {renderArrow(datas.receiptsCount)}
      <Text style={datas.receiptsCount < 0 ? styles.minusPercentage
        : (datas.receiptsCounr === 0
          ? styles.neutralPercentage : styles.plusPercentage)}>
        {datas.receiptsCount
          || datas.receiptsCount === 0
          ? Math.abs(datas.receiptsCount).toString() + '%' : ''}
      </Text>
    </View>
  }
  export const getAverage = (datas)=>{
    return average =
    <View style={{ marginLeft: 0.2 * c.rem, flexDirection: 'row', alignItems: 'flex-end' }}>
      {renderArrow(datas.average)}
      <Text style={datas.average < 0 ? styles.minusPercentage
        : (datas.average === 0
          ? styles.neutralPercentage : styles.plusPercentage)}>
        {datas.average
          || datas.average === 0
          ? Math.abs(datas.average).toString() + '%' : ''}
      </Text>
    </View>
  }

  var styles = EStyleSheet.create({
  plusPercentage: {
    fontFamily: c.RubikR,
    fontSize: '0.55rem',
    color: c.GREEN
  },
  minusPercentage: {
    fontFamily: c.RubikR,
    fontSize: '0.55rem',
    color: c.RED
  },
  neutralPercentage: {
    fontFamily: c.RubikR,
    fontSize: '0.55rem',
    color: c.GREY
  },
  largePlusPercentage: {
    fontFamily: c.RubikR,
    fontSize: '0.7rem',
    color: c.GREEN
  },
  largeMinusPercentage: {
    fontFamily: c.RubikR,
    fontSize: '0.7rem',
    color: c.RED
  },
  largeNeutralPercentage: {
    fontFamily: c.RubikR,
    fontSize: '0.7rem',
    color: c.GREY
  }
})