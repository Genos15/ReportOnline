import {Dimensions, Platform} from 'react-native';
let {width, height} = Dimensions.get('window');

let rem = width > 310 ? (width > 350 ? (width > 390 ? (width > 470 ? (width > 530 ? (width > 590 ? (width > 790 ? (width > 990 ? 60 : 50) : 32) : 28) : 26) : 22) : 18) : 16) : 12;

if (Platform.OS === 'ios') {
  var UbuntuR = 'Ubuntu';
  var UbuntuL = 'Ubuntu-Light';
  var UbuntuB = 'Ubuntu-Bold';
} else {
  UbuntuR = 'Ubuntu-R';
  UbuntuL = 'Ubuntu-L';
  UbuntuB = 'Ubuntu-B';
}
  var RubikR = 'Rubik-Regular';
  var RubikL = 'Rubik-Light';
  var RubikM = 'Rubik-Medium';

module.exports = {
  HEIGHT: height,
  WIDTH: width,
  rem,
  GREEN: '#49a73d',
  RED: '#ef453c',
  DGREY: '#222222',
  GREY: '#717378',
  LGREY: '#f4f4f4',
  LLGREY: '#f1f1f1',
  TGREY: '#c8c7cb',
  BLUE: '#739ee1',
  DEEPBLUE: '#1458c9',
  WHITE: '#ffffff',
  BLACK: '#000000',
  ORANGE: '#ffa332',
  SILVER: '#869dbd',
  LSILVER: '#edeff3',
  UbuntuR,
  UbuntuL,
  UbuntuB,
  RubikM,
  RubikL,
  RubikR
};
