import EStyleSheet from 'react-native-extended-stylesheet';
import c from '../lib/const';

import {
  View,
  TouchableOpacity,
  Text,
  Image,
  Modal,
  BackHandler,
  ImageBackground,
  ScrollView,
  Platform
} from 'react-native';

export default EStyleSheet.create({
  underlayExit: {
    flex: 1,
    backgroundColor: 'rgba(52,52,52,0.5)'
  },
  modalContainerExit: {
    flex: 1,
    marginTop: '10rem',
    marginBottom: '5rem',
    marginLeft: 'rem',
    marginRight: 'rem',
    padding: '1.5rem',
    backgroundColor: c.WHITE,
    borderRadius: '0.4rem'
  },
  icon: {
    height: '2rem',
    justifyContent: 'center',
    alignItems: 'center',
    width: '2rem'
  },
  shopListContainer: {
    paddingBottom: Platform.OS === 'ios' ? c.HEIGHT * 1.5 / 7 : c.HEIGHT / 7,
    paddingLeft: '0.5rem',
    paddingRight: '0.5rem',
    backgroundColor: c.LGREY,
  },
  allShopsContainer: {
    height: c.HEIGHT / 2,
    paddingTop: '0.5rem',
    backgroundColor: c.WHITE,
  },
  shop: {
    height: c.HEIGHT / 11,
    flexDirection: 'row',
    padding: '0.8rem',
    backgroundColor: c.WHITE,
    borderRadius: '0.3rem'
  },
})