import types from '../lib/ActionTypes';
import Hapi from '../lib/hapi';
import { Actions } from 'react-native-router-flux';

export function getStoresSessions(token, item, interval) {
    console.log('getStoresSessions', item, interval)
    return async (dispatch) => {
        Actions.storesSessions()
        dispatch(getStoresSessionsRequest());
        dispatch(getGraphDataRequest());
        dispatch(getShopRequest());
        let response = await Hapi.getStoresSessions(token, item.storeUuid, interval);
        console.log(response);
        let responseGraph = await Hapi.getGraphData(token, item.storeUuid, interval)
        console.log(responseGraph)
        let responseShop = await Hapi.getShop(token, item.storeUuid, interval)
        console.log(responseShop)
        if (response && response.data) {
            dispatch(getStoresSessionsSuccess(response.data, item));
            if (responseGraph && responseGraph.data) {
                dispatch(getGraphDataSuccess(responseGraph.data));
                if (responseShop && responseShop.data) {
                    dispatch(getShopSuccess(responseShop.data));
                } else {
                    dispatch(getShopFailure(responseData));
                }
            } else {
                dispatch(getGraphDataFailure(responseGraph));
            }
        } else {
            Actions.storesSessions()
            dispatch(getStoresSessionsFailure(response, item));
        }
    };
}

function getStoresSessionsRequest() {
    return {
        type: types.GET_STORES_SESSIONS_REQUEST
    };
}
function getStoresSessionsSuccess(data, item) {
    return {
        type: types.GET_STORES_SESSIONS_SUCCESS,
        payload: data, item
    };
}
function getStoresSessionsFailure(error, item) {
    return {
        type: types.GET_STORES_SESSIONS_FAILURE,
        payload: error, item
    };
}
function getGraphDataRequest() {
    return {
        type: types.GET_GRAPH_DATA_REQUEST
    };
}
function getGraphDataSuccess(data) {
    return {
        type: types.GET_GRAPH_DATA_SUCCESS,
        payload: data
    };
}
function getGraphDataFailure(error) {
    return {
        type: types.GET_GRAPH_DATA_FAILURE,
        payload: error
    };
}
function getShopRequest() {
    return {
        type: types.GET_SHOP_REQUEST
    };
}
function getShopSuccess(data) {
    return {
        type: types.GET_SHOP_SUCCESS,
        payload: data
    };
}
function getShopFailure(error) {
    return {
        type: types.GET_SHOP_FAILURE,
        payload: error
    };
}