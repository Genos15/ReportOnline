import { Actions } from 'react-native-router-flux';
import types from '../lib/ActionTypes';
import Hapi from '../lib/hapi';

export function toggleEye() {
  return {
    type: types.TOGGLE_EYE
  };
}

export function setLogin(login) {
  return {
    type: types.SET_LOGIN,
    payload: login
  };
}
export function setPassword(password) {
  return {
    type: types.SET_PASSWORD,
    payload: password
  };
}
export function setFCMToken(token) {
  return {
    type: types.SET_FCM_TOKEN,
    payload: token
  }
}
export function onAuth(login, password) {
  return async (dispatch) => {
    dispatch(authRequest());
    let response = await Hapi.authorize(login, password);
    console.log(response);
    if (response && response.data) {
      console.log('Actions.company')
      Actions.company();
      dispatch(authSuccess(response.data.token));
    } else {
      dispatch(authFailure(response));
    }
  };
}

function authRequest() {
  return {
    type: types.LOGIN_REQUEST
  };
}
function authSuccess(token) {
  return {
    type: types.LOGIN_SUCCESS,
    payload: token
  };
}
function authFailure(error) {
  return {
    type: types.LOGIN_FAILURE,
    payload: error
  };
}