import types from '../lib/ActionTypes';
import Hapi from '../lib/hapi';
import { Actions } from 'react-native-router-flux';

export function getCompany(token, interval) {
    console.log('getCompany')
    return async (dispatch) => {
        dispatch(getCompanyRequest());
        dispatch(getShopListRequest());
        let response = await Hapi.getCompany(token);
        console.log(response);
        let responseCH = await Hapi.getShopList(token, interval);
        console.log(responseCH);
        if (response && response.data) {
            dispatch(getCompanySuccess(response.data));
        } else {
            dispatch(getCompanyFailure(response));
        }
        if (responseCH && responseCH.data) {
            dispatch(getShopListSuccess(responseCH.data));
        }
        else {
            dispatch(getShopListFailure(responseCH));
        }
    };
}

export function getCompanyUpdate(token) {
    console.log('Update Store list');
    return async (dispatch) => {
        dispatch(getCompanyUpdateRequest());
        let response = await Hapi.getCompanyUpdate(token);
        console.log(response);
        if(response && response.data) {
            dispatch(getCompanySuccess(response.data));
        }else {
            dispatch(getCompanyFailure(response));
        }
    };
}

function getCompanyRequest() {
    return {
        type: types.GET_COMPANY_REQUEST
    };
}

function getCompanySuccess(data) {
    return {
        type: types.GET_COMPANY_SUCCESS,
        payload: data
    };
}

function getCompanyFailure(error) {
    return {
        type: types.GET_COMPANY_FAILURE,
        payload: error
    };
}


function getCompanyUpdateRequest() {
    return {
        type: types.GET_COMPANY_UPDATE_REQUEST
    };
}
function getCompanyUpdateSuccess(data) {
    return {
        type: types.GET_COMPANY_UPDATE_SUCCESS,
        payload: data
    };
}
function getCompanyUpdateFailure(error) {
    return {
        type: types.GET_COMPANY_UPDATE_FAILURE,
        payload: error
    };
}

function getShopListRequest() {
    return {
        type: types.GET_SHOP_LIST_REQUEST
    };
}
function getShopListSuccess(data) {
    return {
        type: types.GET_SHOP_LIST_SUCCESS,
        payload: data
    };
}
function getShopListFailure(error) {
    return {
        type: types.GET_SHOP_LIST_FAILURE,
        payload: error
    };
}

export function getAllShopsGraphData(token, interval) {
    console.log('getAllShopsGraphData')
    return async (dispatch) => {
        dispatch(getAllShopsGraphDataRequest());
        let response = await Hapi.getAllShopsGraphData(token, interval);
        console.log(response);
        if (response && response.data) {
            dispatch(getAllShopsGraphDataSuccess(response.data));
        } else {
            dispatch(getAllShopsGraphDataFailure(response));
        }
    }
}

function getAllShopsGraphDataRequest() {
    return {
        type: types.GET_ALL_SHOPS_GRAPH_DATA_REQUEST
    };
}
function getAllShopsGraphDataSuccess(data) {
    return {
        type: types.GET_ALL_SHOPS_GRAPH_DATA_SUCCESS,
        payload: data
    };
}
function getAllShopsGraphDataFailure(error) {
    return {
        type: types.GET_ALL_SHOPS_GRAPH_DATA_FAILURE,
        payload: error
    };
}

export function setModalState(state) {
    return {
        type: types.SET_MODAL_STATE,
        payload: state
    };
}
export function setIntervalState(interval) {
    return {
        type: types.SET_INTERVAL_STATE,
        payload: interval
    };
}
export function setDocumentsIntervalState(interval) {
    return {
        type: types.SET_DOCUMENTS_INTERVAL_STATE,
        payload: interval
    };
}

export function openModal() {
    return {
        type: types.OPEN_MODAL
    };
}

export function closeModal() {
    return {
        type: types.CLOSE_MODAL
    };
}

export function addListOfShops(list) {
    return {
        type: types.ADD_LIST_OF_SHOPS,
        payload: list
    };
}

export function notIsAdd() {
    return {
        type: types.NOT_IS_ADD
    }
}

export function openMenu() {
    return {
      type: types.OPEN_MENU
    };
  }
  
  export function closeMenu() {
    return {
      type: types.CLOSE_MENU
    };
  }