import { Actions } from 'react-native-router-flux';
import types from '../lib/ActionTypes';
import Hapi from '../lib/hapi';


export function getDocuments(token, intervalState) {
  return async (dispatch) => {

    dispatch(documentsRequest());
   // Actions.documents();
    let response = await Hapi.getDocuments(token, intervalState);
    console.log(response);
    if (response && response.data) {

      dispatch(documentsSuccess(response.data));
    } else {
      dispatch(documentsFailure(response));
    }
  };
}

function documentsRequest() {
  return {
    type: types.DOCUMENTS_REQUEST
  };
}
function documentsSuccess(data) {
  return {
    type: types.DOCUMENTS_SUCCESS,
    payload: data
  };
}
function documentsFailure(error) {
  return {
    type: types.DOCUMENTS_FAILURE,
    payload: error
  };
}

export function fromDocuments() {
  return {
    type: types.FROM_DOCUMENTS
  }
}

export function addArrayChecks(paybacks, receipts) {
  return {
    type: types.ADD_ARRAY_CHECKS,
    paybacks: paybacks,
    receipts: receipts
  }

}