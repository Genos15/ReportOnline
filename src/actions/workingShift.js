import types from '../lib/ActionTypes';
import Hapi from '../lib/hapi';
import { Actions } from 'react-native-router-flux';

export function getShopChecks(token, item, interval, session) {
    console.log('getShopChecks', item, session)
    return async (dispatch) => {
        Actions.workingShift()
        dispatch(getShopChecksRequest());
        let response = await Hapi.getShopChecks(token, item.storeUuid, interval, session.sessionNumber);
        console.log(response);
        if (response && response.data) {
            dispatch(getShopChecksSuccess(response.data, session));
        } else {
            dispatch(getShopChecksFailure(response, session));
        }
    };
}

function getShopChecksRequest() {
    return {
        type: types.GET_SHOP_CHECKS_REQUEST
    };
}
function getShopChecksSuccess(data, item) {
    return {
        type: types.GET_SHOP_CHECKS_SUCCESS,
        payload: data, item
    };
}
function getShopChecksFailure(error, item) {
    return {
        type: types.GET_SHOP_CHECKS_FAILURE,
        payload: error, item
    };
}

export function setSelectTab(data) {
    return {
        type: types.SET_SELECT_TAB,
        payload: data
    };
}
export function getCheck(selectCheck) {
    Actions.check()
    return {
        type: types.OPEN_CHECK,
        payload: selectCheck
    };
}
