import * as companyActions from './company';
import * as workingShiftActions from './workingShift';
import * as authActions from './auth';
import * as sessionActions from './session';
import * as storesSessionsActions from './storesSessions';
import * as documentsActions from './documents'

export const ActionCreators = Object.assign({},
    companyActions,
    workingShiftActions,
    authActions,
    sessionActions,
    storesSessionsActions,
    documentsActions
);