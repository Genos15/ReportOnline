import types from '../lib/ActionTypes';
import { sessionToken } from '../lib/SessionToken';
import { Actions } from 'react-native-router-flux';

export function saveSession(state) {
  var newSession = Object.assign({}, state, {
    state
  });
  sessionToken.saveSession(newSession);
  return {
    type: types.SAVE_SESSION
  };
}

export function getSession() {
  console.log('geting session token');
  return dispatch => {
    dispatch(getSessionRequest());
    return sessionToken.getSession()
      .then((response) => {
        console.log('responseeee', response);
        if (response.session) {
          dispatch(setSessionToState(response.session));
        } else {
          console.log('no token');
          Actions.auth();
        }
      })
      .catch((error) => {
        dispatch(getSessionFailure(error));
        Actions.auth();
      });
  };
}

function setSessionToState(session) {
  if (session.token) {
    if (session.lastWindow === 'company') {
      Actions.company();
    } else {
      Actions.documents();
    }

  }
  return {
    type: types.SET_SESSION_TO_STATE,
    payload: session
  };
}

export function onLogout() {
  console.log('geting session token');
  return dispatch => {
    dispatch(deleteSessionRequest());
    return sessionToken.deleteSession()
      .then(() => {
        dispatch(deleteSessionSuccess());
        Actions.auth();
      })
      .catch((error) => {
        dispatch(deleteSessionFailure(error));
      });
  };
}

function getSessionRequest() {
  return {
    type: types.GET_SESSION_REQUEST
  };
}
function getSessionFailure(error) {
  return {
    type: types.GET_SESSION_FAILURE,
    payload: error
  };
}
function deleteSessionRequest() {
  return {
    type: types.DELETE_SESSION_REQUEST
  };
}
function deleteSessionSuccess() {
  return {
    type: types.DELETE_SESSION_SUCCESS
  };
}
function deleteSessionFailure(error) {
  return {
    type: types.DELETE_SESSION_FAILURE,
    payload: error
  };
}
